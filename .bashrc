#!/bin/bash

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# run all common scripts
if [[ -d $HOME/.bashrc.d ]] ; then
    for FILE in $HOME/.bashrc.d/*.sh ; do
        source "$FILE"
    done
fi

# run all scripts that contain sensitive information and thus may not be shared publicly
if [[ -e $HOME/.bashrc.local ]] ; then
    source $HOME/.bashrc.local
fi

