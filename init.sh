echo Uncomment the following line to continue.

exit 0

linkit() {
    local FILE=$1
    if [[ -e ~/$1 ]] ; then
        echo WARNING: File '$FILE' exists. Skipping.
    else
        echo Linking $FILE
        ln -s $HERE/$FILE $HOME/$FILE
    fi
}

HERE=$(dirname $(readlink -f $0))
cd
if [[ -e .bashrc.old ]] ; then
    echo A file called .bashrc.old was found.
    echo To be safe, this script will now terminate.
    return 0
else
    mv .bashrc .bashrc.old
    mv .bash_profile .bash_profile.old
    linkit .bashrc
    linkit .bashrc.d
    linkit .tmux.conf
    linkit .vimrc
    linkit .vimrc-plug
    linkit .kitty
    linkit .alacritty.toml
    linkit .gitconfig
fi
