Notes
=====

* The font settings in this folder all produce more or less the same font density.

* Offset determines the extra white space between glyphs

* Negative fond.glyph_offset.y is needed for some fonts when using a negative value of font.offset.y to prevent slight clipping of the glyphs at the complete top of the window. It might be noticed on most numbers and 'high' glyphs like the letter 'f'::

    font.glyph_offset:
    x = 0
    y = -1

* To list font names, run "fc-list | sort"

* A mounted Windows 10 installation is needed for lucida console and consolas packages.

Font packages used::

    sudo pacman -S ttf-hack ttf-liberation ttf-dejavu ttf-inconsolata otf-cascadia-code-nerd adobe-source-code-pro-fonts ttf-fira-code
    aurto add otf-monego-git ttf-meslo ttf-ms-win10-auto
    sudo pacman -Syu otf-monego-git ttf-meslo ttf-ms-win10-auto

AUR manual build, due to automated build failing, because SF-Mono.dmg checksum is variable::

    cd /tmp
    git clone https://aur.archlinux.org/otf-apple-sf-mono.git
    cd otf-apple-sf-mono
    makepkg -g >> PKGBUILD
    makepkg -si

Manual install because no package available - https://rvklein.me/proj/binchotan/ ::

    sudo -i
    cd /usr/local/share/fonts
    wget https://rvklein.neocities.org/proj/binchotan/build/Binchotan_Sharp__400__(v_000_526)_.ttf
    fc-cache -fv

References::

    https://sourcefoundry.org/hack/playground.html
    https://www.nerdfonts.com
    https://www.programmingfonts.org
