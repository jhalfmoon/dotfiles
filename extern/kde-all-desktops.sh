#!/bin/bash -eux

# Source: https://github.com/hnjae/kwin-scripts.git
#
# About the script: When the script is enabled, all windows placed on monitors other than
# the primary, are automatically set to be shown on all virtual desktops.

NAME=kwin-scripts

cd /tmp/
git clone https://github.com/hnjae/$NAME.git
cd $NAME
kpackagetool5 -r virtual-desktops-only-on-primary

#install()   { kpackagetool5 -i "$scriptName" }
#uninstall() { kpackagetool5 -r "$scriptName" }
#upgrade()   { kpackagetool5 -u "$scriptName" }
