" This is .vimrc
"
" jhalfmoon 20140428
"
" Based on example config:
"   https://github.com/terryma/dotfiles/blob/master/.vimrc
"   https://github.com/bling/dotvim
" TODO
"       Make tabularize leaderkey dependent on filetype (=> for puppet, = for bash)
"
" TIPS
" - PluginList          list all active plugins
" - help <plugin-name>  show plugin help
" - syntasticinfo       show info on current syntac checking
" - tabularize block    align puppet hashrockets
" - lnext, lprev        jump between errors
" - tagbartoggle        open/close tagbar
" - ,nt              toggle nerdtree
" - ,ag              align block
" - ,b               buffer explorer (ctrlp is used)
" - ,c               toggle comment
" - :FixWhiteSpace   to fix trailing spaces
" - Folding -  http://vim.wikia.com/wiki/Folding
"       zm           fold all
"       zr           unfold all
" - %                jump between delimiters

filetype plugin on
filetype indent on

" The following settings must be set before setting colorschemes. If not done, then colorschemes
" will get loaded incorrectly. Try doing 'colorscheme kruby' before this is set and see the effect.
syntax enable
" enable 256 color mode
set t_Co=256

" Source vim-plug config if vim-plug plugin and its config file are available
" Check the vimrc-plug file for installation instructions
if filereadable(expand("$HOME/.vim/autoload/plug.vim"))
    if filereadable(expand("$HOME/.vimrc-plug"))
      source $HOME/.vimrc-plug
    endif
endif

" This enables you to put your cursor on one parenthesis, or bracket, or curly brace,
" type %, and then your cursor will automatically jump to the other element of the pair.
runtime macros/matchit.vim

" highlight Normal guibg=black guifg=white
" highlight Normal ctermbg=None
" highlight Normal ctermbg=black
" set background=light

" remap keys to better match colemak layout
noremap j k
noremap k j

" Press enter to clear search highlights - from https://stackoverflow.com/questions/657447/vim-clear-last-search-highlighting
nnoremap <CR> :noh<CR><CR>

" Remap F1 to escape - no more accidental helptext
map <F1> <Esc>
imap <F1> <Esc>

set paste

"set mouse=a
set nocompatible                " this actually gets set by default if ~/.vimrc is present
set encoding=utf-8              " force to utf8 mode

set tabstop=4
set softtabstop=4
set shiftwidth=4
set smarttab
set expandtab
set modeline
set modelines=5

set nonumber                    " disable all linenumbering by default
set norelativenumber
set ruler                       " show cursor line and column in the status line
"set cursorline                  " highlight the screen line of the cursor
set ignorecase                  " most of the time, we don't care for sensitive searches
set showmatch                   " briefly jump to matching bracket if insert one
set hlsearch                    " highlight matches with last search pattern
set incsearch                   " highlight match while typing search pattern
set scrolloff=3                 " Keep 3 lines below and above the cursor
set backspace=indent,eol,start  " makes backspace key more powerful.
set formatoptions-=cro          " do not autowrap or autoinsert comment leader (see 'help fo-table')
set nobackup                    " keep backup file after overwriting a file
set autoread                    " Set to auto read when a file is changed from the outside

" force search highlighting to something I like
hi Search cterm=NONE ctermfg=black ctermbg=yellow

" Open all folds initially
set foldmethod=indent
set foldlevelstart=99

" Text display settings
set wrap
set whichwrap+=h,l,<,>,[,]

" Always splits to the right and below
set splitright
set splitbelow

" Display unprintable chars
" set list
" set listchars=tab:▸\ ,extends:❯,precedes:❮,nbsp:␣
" set showbreak=↪

" listchar=trail is not as flexible, use the below to highlight trailing
" whitespace. Don't do it for unite windows or readonly files
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
augroup MyAutoCmd
  autocmd BufWinEnter * if &modifiable && &ft!='unite' | match ExtraWhitespace /\s\+$/ | endif
  autocmd InsertEnter * if &modifiable && &ft!='unite' | match ExtraWhitespace /\s\+\%#\@<!$/ | endif
  autocmd InsertLeave * if &modifiable && &ft!='unite' | match ExtraWhitespace /\s\+$/ | endif
  autocmd BufWinLeave * if &modifiable && &ft!='unite' | call clearmatches() | endif
augroup END

" NOTE 20140503 This bit is commented out because it causes konsole to reset to default
" zoom level when insert mode is entered.
" Cursor settings. This makes terminal vim sooo much nicer!
" Tmux will only forward escape sequences to the terminal if surrounded by a DCS
" sequence
" if exists('$TMUX')
"   let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
"   let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
" else
"   let &t_SI = "\<Esc>]50;CursorShape=1\x7"
"   let &t_EI = "\<Esc>]50;CursorShape=0\x7"
" endif

" When editing a file, always jump to the last cursor position
au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal g'\"" | endif

if has ("autocmd")
    au FileType cpp,c,java,sh,pl,php,phtml,asp,xml,javascript,python set smartindent
    autocmd FileType text set nonumber norelativenumber

    au FileType pp nmap <leader>a :Tabularize /=> <CR>
"    au FileType pp nmap <leader>c :s/^/\/\/:s/^\/\/\/\// <CR>
"    au FileType pp nmap <leader>c :s/^/# <CR>
endif

"=== shortcuts ===========================================
let mapleader = ","
nmap       <leader>pp   :set paste! <cr>
nnoremap   <leader>tn   :tabnext<CR>
nnoremap   <leader>tp   :tabNext<CR>
nnoremap   <leader>n    :tabnew<CR>
nnoremap   <leader>ww   :%s/\s\+$//g<CR>
map        <leader>q            ysiw'
map        <leader><leader>q    ysiw"
map <space> 10j
map #<space> 10k

"=== shortcuts , these settings depend on certain plugins being installed
"" TODO remove this if unite can deliver what is promised
" nnoremap   <leader>p    :CtrlP <CR>
" nnoremap   <leader>b    :CtrlPBuffer <cr>
" nmap       <leader>f    :Ack <cr>
nmap       <leader>nt   :NERDTreeToggle <cr>
map        <leader>c    :TComment <cr>
nnoremap <silent> <Leader>tb :TagbarToggle<cr>
" nnoremap <silent> <F9> :TagbarToggle<CR>

"===

" Like gJ, but always remove spaces
fun! s:join_spaceless()
    execute 'normal! gJ'

    " Remove character under the cursor if it's whitespace.
    if matchstr(getline('.'), '\%' . col('.') . 'c.') =~ '\s'
        execute 'normal! dw'
    endif
endfun

" Map it to a key
nnoremap <Leader>J :call <SID>join_spaceless()<CR>

