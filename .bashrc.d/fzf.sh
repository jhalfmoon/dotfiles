
if ! (which fzf &>/dev/null) ; then
    echo "NOTE: fzf is not installed. Check $(ls $HOME/.bashrc.d/*fzf*) for instructions."
else
    # set up key bindings and completion
    # Note: On most distros, this done by sourcing key-bindings.bash and completion.bash.
    #       This command generates the same as is contained in those file.
    eval "$(fzf --bash)"
fi

# Howto install:
#   BIN=$HOME/bin
#   mkdir -p $BIN &>/dev/null
#   git clone --depth 1 https://github.com/junegunn/fzf.git $BIN/fzf.git
#   $BIN/fzf.git/install --bin
#   ln -s $BIN/fzf.git/bin/fzf $BIN/fzf
# or from:
#   https://github.com/junegunn/fzf/releases

# From: https://betterprogramming.pub/boost-your-command-line-productivity-with-fuzzy-finder-985aa162ba5d
export FZF_DEFAULT_OPTS="
--exact
--layout=reverse
--info=inline
--height=50%
--border
--multi
--preview-window=:hidden
--preview '([[ -f {} ]] && (bat --style=numbers --color=always {} || cat {})) || ([[ -d {} ]] && (tree -C {} | less)) || echo {} 2> /dev/null | head -200'
--color='hl:148,hl+:154,pointer:032,marker:010,bg+:237,gutter:008'
--prompt='∼ ' --pointer='▶' --marker='✓'
--bind '?:toggle-preview'
--bind 'ctrl-a:select-all'
--bind 'ctrl-y:execute-silent(echo {+} | pbcopy)'
--bind 'ctrl-e:execute(echo {+} | xargs -o vim)'
--bind 'ctrl-v:execute(code {+})'
"

