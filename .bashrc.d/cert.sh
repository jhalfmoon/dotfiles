# Certificate tooling

csr-info() {
    openssl req -text -noout -in $1 | less
}

crt-info() {
    openssl x509 -text -in $1 | less
}

# key modulus shortcuts
alias crt-mod='openssl x509 -modulus -noout -in'
alias key-mod='openssl rsa -modulus -noout -in'
alias csr-mod='openssl req -modulus -noout -in'

# Dump certificate chain from site. Usage:
# -a           Dump all certificates instead of just the host certificate
# -s <vhost>   Get certificate for $vhost
# Example: crt-dump -a -s some.vhost.com foo.local:443
#
crt-dump() {
    local OPTIND ARGS HOST RAWDATA DETAILS
    FLAG_ALLCERTS=0

    [[ $# -lt 1 ]] && echo "ERROR: At least one parameter must be given: \$HOST:\$PORT" && return 1
    while getopts "as:w" options; do
        case $options in
            a)  ARGS="$ARGS -showcerts"
                ;;
            s)  ARGS="$ARGS -servername $OPTARG"
                ;;
        esac
    done
    shift $((OPTIND-1))
    HOST=$1

    RAWDATA=$(echo | openssl s_client -connect $HOST ${ARGS[@]} 2>&1)
    DETAILS=$(echo "$RAWDATA" | openssl x509 -noout -text)
    echo "$RAWDATA"
    echo "$DETAILS"
}

# Create csr from cert
#openssl x509 -x509toreq -in fd.crt -signkey fd.key -out fd.csr

# mTLS testing using openssl
#   echo | openssl s_client -CAfile $CERTS/ca-crt.pem -cert $CERTS/client-crt.pem -key $CERTS/client-key.pem -connect foo.local:9100

# mTLS testing using curl
#    curl --noproxy '*' -kv --cacert $CERTS/ca-crt.pem --cert $CERTS/client-crt.pem --key $CERTS/client-key.pem https://foo.local:9100

