export LIBVIRT_DEFAULT_URI="qemu:///system"
export VAGRANT_DEFAULT_PROVIDER=libvirt
export VAGRANT_CHECKPOINT_DISABLE=1
export VAGRANT_BOX_UPDATE_CHECK_DISABLE=1
export VAGRANT_I_KNOW_WHAT_IM_DOING_PLEASE_BE_QUIET=true

alias vm='vboxmanage list runningvms'
alias vl='virsh list --all'
#alias v='virsh --connect qemu:///system'

vpop() {
    set -x
    NODE=$(virsh list --all | grep -A1 -- '----' | awk '{print $2}')
    if [ ! -z "$NODE" ] ; then
        virsh shutdown --domain $NODE
        virsh snapshot-revert --domain $NODE --snapshotname snap1
        virsh start --domain $NODE
    fi
    set +x
}

vpush() {
    set -x
    NODE=$(virsh list --all | grep -A1 -- '----' | awk '{print $2}')
    if [ ! -z "$NODE" ] ; then
        virsh shutdown --domain $NODE
        virsh snapshot-create-as --domain $NODE --name "snap1" --description "My First Snapshpot"
        sleep 3
        virsh start --domain $NODE
    fi
    set +x
}

alias v='vagrant'
alias vh='vagrant halt'
alias vst='vagrant status'
alias vsnap='vagrant snapshot save'
alias vr='vagrant snapshot restore --no-provision'
alias c6="cd $HOME/b/v/c6 ; vagrant up ; vagrant ssh"
alias c7="cd $HOME/b/v/c7 ; vagrant up ; vagrant ssh"
alias cc7="cd $HOME/b/v/c7"

# A 'vagrant ssh' replacement command that actually works under Windows 10
vs() {
    if [ $# -eq 0 ] ; then
        vagrant ssh
    else
        NODE=$1
        shift
        PARM=$@
        CONFIG=/tmp/$NODE.conf
        vagrant ssh-config $NODE > $CONFIG
        ssh -F $CONFIG $PARM $NODE
    fi
}

vu() {
    for KEY in "build/vagrant-ssh-key" "base/vagrant-ssh-key" ; do
        if [ -f $KEY ] ; then
            ssh-add $KEY || echo "Failed to add key $KEY"
        fi
    done
    vagrant up $@
#    if vagrant up $@ ; then
#        find .vagrant | grep private | xargs ssh-add
#        vagrant ssh $1
#    fi
}

#alias vd='vagrant destroy -f'
vd() {
    # ssh-add wants the pubkey to be able to delete a key from its cache, so generate one before deleting the key
    # and th alternative is to delete all keys with 'ssh-add -D'
    ssh-add -L | grep 'virtualbox/private_key\|vagrant-ssh-key' | cut -d' ' -f3 | while read FILE; do
        if [ -e "$FILE" ] ; then
            ssh-keygen -yf $FILE > $FILE.pub
            ssh-add -d $(readlink -f $FILE.pub)
        else
            echo "WARNING: Private key $FILE is not found. Unable to remove key from agent."
        fi
    done
    vagrant destroy $@ -f
    # Destroy the network to wipe any leaese. If this is not done libnss resolution will get impossibly slow
    # due to the accumulation of dhcp leases of hosts with identical names, slowing down ssh connections.
    # Check 'virsh net-dhcp-leases vagrant-libvirt'
    virsh net-destroy vagrant-libvirt
}

