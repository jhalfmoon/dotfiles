# NOTE: This file must be named such that it always ends up last in a directory when sorted alfabetically.

# Add extra linefeed to prompt to always have a 'clean' / UTF
# Note: This must be done after PS1 is fully customized, so make it the last command in your .bashrc
PS1="\n$PS1"

# update history file after each command
PROMPT_COMMAND="history -a ; $PROMPT_COMMAND"

