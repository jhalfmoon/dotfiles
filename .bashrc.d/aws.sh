astatus() {
    aws ec2 describe-instances --query 'Reservations[*].Instances[*].[Placement.AvailabilityZone, State.Name, InstanceId, PrivateIpAddress, PublicIpAddress]' --output text | grep eu-central-1
    #aws ec2 describe-instances | grep -i instanceid | cut -d: -f2 | cut -d\" -f2
}

astart() {
    if [[ -z $1 ]] ; then
        LIST=$(astatus | grep stopped | awk '{print $3}')
    else
        LIST=$(astatus | grep stopped | awk '{print $3}' | head -n $1)
    fi
    if [ -z "$LIST" ] ; then
        echo "NOTE: No startable instances found."
    else
        aws ec2 start-instances --instance $LIST
    fi
}

astop() {
    if [[ -z $1 ]] ; then
        LIST=$(astatus | grep running | awk '{print $3}')
    else
        LIST=$(astatus | grep running | awk '{print $3}' | head -n $1)
    fi
    if [ -z "$LIST" ] ; then
        echo "NOTE: No stopable instances found."
    else
        aws ec2 stop-instances --instance $LIST
    fi
}

aa() {
    if [ $# -eq 0 ] ; then
        INDEX=1
    elif [ $# -eq 1 ] ; then
        INDEX=$1
    else
        echo "ERROR: More that one argument supplied"
        return 1
    fi
    LIST="$(astatus | grep running | awk '{print $5}')"
    LISTSIZE=$(echo -n "$LIST" | wc -w)
    IP=$(echo "$LIST" | head -n $INDEX | tail -n1 )
    echo $IP
    if [ $INDEX -gt $LISTSIZE ] ; then
        echo "ERROR: Index ($INDEX) is larger than list of host ($LISTSIZE)"
        return 1
    else
        ssh -p 443 ubuntu@$IP
    fi
}

