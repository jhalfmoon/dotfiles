alias t="tmux attach || tmux new-session -n mine"
alias h="rm -rf public ; hugo -b /cocktails ; hugo server --buildDrafts --ignoreCache --disableFastRender --bind 0"

alias ytl='yt-dlp -F'
alias yta='yt-dlp --audio-quality 10'
#alias yta='yt-dlp -f251'
alias ytv='yt-dlp -f249+135'

#alias yta='youtube-dl --extract-audio --audio-format mp3 -f bestaudio'
#alias ytv='youtube-dl -f bestvideo'

# NOTE: 'ccd' is a shell function that allows you to cd to a file's directory, instead of just a directory
alias cd='ccd'
alias vi='vim'
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'
alias df="df -h"
alias jc='journalctl --boot -o short'
alias jx='journalctl -o short'
#alias less='less -RSXF'
alias ls='ls --color=auto'
alias l='ls -lh --color=auto'
alias ll='ls -alh --color=auto'
alias lt='ls -lhtr --color=auto'
alias nt='ss -plnt|column -t|sort -nk4'
alias nu='ss -plnu|column -t|sort -nk4'
# remove a certain key from known_hosts
alias kkey="ssh-keygen -R"

lscat() {
    ls $pwd | while read ding ; do
        echo -n "$ding : "
        cat $ding
    done | column -t
}

# Remove non-needed packages
alias pac-clean='sudo pacman -Rs $(pacman -Qdt|cut -d" " -f1)'
# list unmanaged packages, ie. install with pacman -U
alias pac-manual='pacman -Qm'
alias pu='pacman -Syu --noconfirm ; sync'
# Show explicitely installed packages
alias pac-explicit='pacman -Qqe > pkglist.txt'
# Show largest packages
alias pac-size="pacman -Qi | grep '\(Version\|Name\|Installed Size\)\W\+:\|^$' | sed 's/^$/@/g' | cut -d':' -f2- | tr '\n' '#' | tr '@' '\n' | sed 's/#/    /g' | sed 's/ \+/ /g' |sort -nk3 | grep MiB | column -t"

# no-proxy
alias np="unset http_proxy https_proxy HTTP_PROXY HTTPS_PROXY"
# show-proxy
alias sp='echo -e "Proxies:" ; env | grep -i http; export | grep -i http'

alias gitupa='find -maxdepth 1 -type d |while read ding;do cd $ding;git checkout master;git pull;git checkout -;cd -;done'
alias curlh='curl -vLo /dev/null'
alias venv='source ~/.venv/bin/activate'

alias avc='cat /var/log/audit/audit.log | grep -i avc'

# make sudo available to execute aliased commands
# http://askubuntu.com/questions/22037/aliases-not-available-when-using-sudo
alias sudo='sudo '

# XXX: The umask bit still doesnt work. I need to fix this but haven't the time right now.
#alias selfsign='export DATE=$(date +%Y%m%d-%H%M) ; sh -c "(umask 066; openssl req -new -newkey rsa:2048 -x509 -nodes -days 900 -keyout $DATE.key -out $DATE.crt -subj \"/C=NL/ST=Noord Hollad/L=Hilversum/C=NL/O=NPO/OU=none/CN=*.zappyourplanet.nl\")"'
alias phpinfo="echo '<?php phpinfo(); ?>'"
alias htpasswd2='openssl passwd -crypt'

# sort IP numbers ; https://www.madboa.com/geek/sort-addr/
alias ipsort='sort -n -t . -k 1,1 -k 2,2 -k 3,3 -k 4,4'
alias lm='lsmod|sort|grep -v Module|cut -d" " -f1|column -t'
alias psme='ps aux|grep `whoami`|sort -k9'
alias il='iptables --line-numbers -nvL'

alias sockit="LD_PRELOAD=/usr/lib/torsocks/libtorsocks.so:$LD_PRELOAD"
alias sockit2="TSOCKS_CONF_FILE=$HOME/.sockit.conf LD_PRELOAD=/usr/lib/torsocks/libtorsocks.so:$LD_PRELOAD"
alias tunnelmake='proxytunnel -v -a 1200 -p 10.76.18.5:8080 -d thuis.milksnot.com:443 &'
alias vpn1="autossh -f -2 -N -M 0 -o TCPKeepAlive=yes -o ServerAliveInterval=180 thuis"

# use to re-enable an unplugged USB audio device
alias eu="killall -9 pulseaudio;sleep 0.5;pulseadio \&;amixer -c1 sset PCM unmute 100%"

# decode base64 string
dec() {
    echo $1 | base64 -d ; echo
}

# Create a temp dir and cd to it
tt() {
    local ACTION=$1
    # The folder name is selected to always appear on top in a file selector
    local BASEDIR=/tmp/.0
    # Create the base dir if it does not exist and initialize the index counter
    if [[ ! -d $BASEDIR ]] ; then
        mkdir -p $BASEDIR
        local INDEX=1
        local FILE_COUNT=0
    else
        local INDEX=$(cat $BASEDIR/index)
        local FILE_COUNT=$(ls $BASEDIR/a${INDEX} | wc -l)
    fi
    if [[ "$ACTION" == 'up' ]] && [[ $FILE_COUNT -ne 0 ]] ; then
        # Create a new temp dir if requested and current temp dir is not empty
        INDEX=$((INDEX+1))
    elif [[ "$ACTION" == 'down' ]] && [[ $INDEX -ne 1 ]] ; then
        # Jump to the previous temp dir if requested and one exists
        INDEX=$((INDEX-1))
    fi
    # Store the index so that it can be used during the next call of this function
    echo $INDEX > $BASEDIR/index
    local DIR=$BASEDIR/a${INDEX}
    if [[ ! -d $DIR ]] ; then
        mkdir $DIR
    fi
    cd $DIR
}

# Create a new temp dir and switch to it
alias tn="tt up"
# Go to the previous temp dir
alias tp="tt down"

# make a dir and CD to it
md() { mkdir -p "$1" ; cd "$1" ; }
# du++
dux() { du -xcsh ${@:-*} | sort -h ; }
# f(ind)f(ile) - find file recursive, highlight, pager
ff() { find -type f -iname "*$1*" | grep -v '.svn$' | grep -i --color=always "[^/]*$1[^/]*\$" | less -RXSF ; }
# f(ind)in $1 $2 - find all files with $1 in name and search for text $2 in them
fin() { find -type f -iname "*$1*" | grep -v '.svn$' | while read FILE ; do grep -iH --color=always "$2" "$FILE" ; done | less -RXSF ; }
# f(ind)d(ir) - find dir recursive, highlight, pager
fd() {
    BASE=`pwd`/ ; [ "$#" -eq 2 ] && BASE="$2"
    find "$BASE" -type d -iname "*$1*" 2> /dev/null | sed 's@ \.@ '$BASE'@g' | column -t | grep -i --color=always "[^/]*$1[^/]*\$" | less -RXSF
}

# find a string recursively in the cwd
a() {
    BASE=`pwd`/ ; [ "$#" -eq 2 ] && BASE="$2"
    if (which ag &>/dev/null) ; then
        ag --color --ignore-case --heading --hidden "$1" "$BASE" 2> /dev/null | less -RXSF
    else
        grep --recursive --color=always --ignore-case "$1" "$BASE" 2> /dev/null | less -RXSF
    fi
}

# tree with files
tf () { $(which tree) -C $@ | less -RXF ;}

# tree dirs only
td () { $(which tree) -dC $@ | less -RXF ;}

# shortcut for editing scripts located in $PATH
viw() { (which $1 &> /dev/null) && vim $(which $1) || vim $1;}

# columnized ss (netstat replacement)
nss() { ss $@ | column -t ;}

mydate() { date +%Y%m%d-%H%M ; }

# cd to the parent dir of a given file
ccd() {
    if [ $# -eq 0 ] ; then
        \cd
    elif [ "$1" != '-' ] && [ ! -d "$1" ] ; then
        \cd `dirname "$1"`
    else
        \cd "$1"
    fi
}

# basic error reporting
fail() {
    echo "$1"
    exit 1
}

# timestamp generator
nano() { date +%s:%N ; }

# display IP adresses
# remove protocol prefix and path suffix
myhost() {
    NAME=${1#*://}
    NAME=${NAME%%/*}
    echo $NAME
}

nicinfo() {
    if which ip > /dev/null 2>&1 ; then
        ip addr | grep -e 'inet ' | grep 'eth\|wlan\|bond' | sed 's/^[ \t]\+//g' | sed 's/ brd [0123456789\.]\+//g' | while read LINE; do
            ADDR=$(echo $LINE | awk '{print $2}')
            IFACE=$(echo $LINE | grep -o '\w\+$')
            echo "$IFACE : $ADDR"
        done | column -t
    fi
}

# Allows a copy-paste of a GNU make output line and opens the source file at the given coordinates
vimsrc() {
    FILE=`echo $1 | cut -d':' -f1`
    LINE=`echo $1 | cut -d':' -f2 | grep -v '[^0-9]'`
    COL=`echo $1 | cut -d':' -f3 | grep -v '[^0-9]'`
    [ "$LINE" == "" ] && LINE=0
    [ "$COL" == "" ] && COL=0
    vim "+call cursor($LINE,$COL)" "$FILE"
}

# force a group of processes to ionice idle
makenice() { ps aux | grep -i $1 | awk '{print $2}' | while read ding ; do ionice -p $ding ; done ;}

# readhex ; reads a range of memory from $1 with size $2
rh() { dd if=/dev/mem bs=4 skip=$((0X$1/4)) count=$((0x$2/4)) | hexdump -C  ;}

# acpi call wrapper; does nothing without proper parameters
da() { echo "\\$1" | sudo tee /proc/acpi/call >/dev/null && sudo cat /proc/acpi/call;echo ;}

myhtpasswd() {
    echo "Enter username <space> password"; read name passwd toomuch
    if [ "$passwd" == "" ] || [ ! "$toomuch" == "" ] ; then
        echo "ERROR: This script requires exactly 2 paramaters : username ."
        return 1
    fi
    echo $name:$(openssl passwd -crypt $passwd 2>/dev/null) | tee -a .htpasswd
}

dnstest() {
    echo "################"
    echo -n '==localhost:'
    time dig +short $1 localhost
    echo -n '==router:'
    time dig +short $1 10.1.1.1
}

fixfs() {
    # This tool is to fix the 'duplicate UUID' error that may occur
    # when an XFS partition gets removed before being unmounted.
    PART=$1
    if [ "$PART" = '' ] ; then
        echo "Error: No device name given."
        echo "Please indicate an XFS partition that you wish to fix."
    else
        mkdir /tmp/tmp_mount
        mount -o nouuid $PART /tmp/tmp_mount
        umount $PART
        xfs_admin -U generate $PART
    fi
}

# Show all loaded modules and their parameters
function modparms ()
{
    N=/dev/null;
    C=`tput op` O=$(echo -en "\n`tput setaf 2`>>> `tput op`");
    for mod in $(cat /proc/modules|sort|cut -d" " -f1);
    do
        md=/sys/module/$mod/parameters;
        [[ ! -d $md ]] && continue;
        m=$mod;
        d=`modinfo -d $m 2>$N | tr "\n" "\t"`;
        echo -en "$O$m$C";
        [[ ${#d} -gt 0 ]] && echo -n " - $d";
        echo;
        for mc in $(cd $md; echo *);
        do
            de=`modinfo -p $mod 2>$N | grep ^$mc 2>$N|sed "s/^$mc=//" 2>$N`;
            echo -en "\t$mc=`cat $md/$mc 2>$N`";
            [[ ${#de} -gt 1 ]] && echo -en " - $de";
            echo;
        done;
    done
}

# show a summary of CPU related info
cpuinfo() {
    STRIP='cut -d"=" -f2 | tr "\n" ","'
    INFO=$(cat /proc/cpuinfo|sort|uniq|sed 's@\(\s*\w\)\s*:\s*\(.*\)@\1=\2@g')
    NAME=$(echo "$INFO" | grep 'model name' | eval $STRIP | sed 's/(TM)\|(R)\|CPU //g')
    SOCKETCOUNT=$(echo "$INFO" | grep 'physical id' | cut -d"=" -f2 | tail -n1)
    CORECOUNT=$(echo "$INFO" | grep 'cpu cores' | eval $STRIP)
    SIBLINGS=$(echo "$INFO" | grep 'siblings' | eval $STRIP)
    CACHESIZE=$(echo "$INFO" | grep 'cache size' | eval $STRIP)
    FAMILY=$(echo "$INFO" | grep 'cpu family' | eval $STRIP)
    LEVEL=$(echo "$INFO" | grep 'cpuid level' | eval $STRIP)
    echo "${NAME%,} : $((SOCKETCOUNT+1))x${SIBLINGS%,} : ${CACHESIZE%,}"
    #echo "${NAME%,} (F${FAMILY%,}:L${LEVEL%,}) : $((SOCKETCOUNT+1))x${SIBLINGS%,} : ${CACHESIZE%,}"
}

# Usage: addekey <key shortname> <full key path>
# Thes shortname is used to set the terminal name during password query.
# http://www.funtoo.org/Keychain
addkey() {
    KEYCHAIN='keychain --eval --nogui --quiet --attempts 3'
    if [ -s $2 ] && [ `whoami` != 'root' ] && which keychain > /dev/null 2>&1 ; then
        # set terminal title, to enable autotyping for passwordmanagers
        echo -ne "\033]0;ssh-key-$1\007"
        shift
        eval `$KEYCHAIN $@`
    fi
}

# This function was created to be able to call it using 'time' , to determine how long it takes.
# For testpurposes only. If needed , the code can be removed from the function and called directly.
call_addkeys() {
    echo "Adding ssh keys to ssh-agent..."
    KEYROOT='/1/opslag/keys'
    addkey mine \
        "$KEYROOT/ssh/example-key1" \
        "$KEYROOT/ssh/example-key2"
}

setColorName() {
    #echo "$1 - $2"
    #eval $1=\''\[\033['$2'm\]'\'
    eval $1=\''\033['$2'm'\'
}

# Usable color names:
#  ${_color}   normal color
#  ${__color}  bold / bright
#  ${___color} dim color
#  ${_reset}   reset style
generateColors() {
    #_reset='\[\033[0m\]'
    _reset='\033[0m'
    names="black red green yellow blue magenta cyan white"
    index=30
    for name in $names; do
        setColorName _${name}   "0;$index"
        setColorName __${name}  "1;$index"
        setColorName ___${name} "2;$index"
        ((index++))
    done
    echo -e $_red red
}

