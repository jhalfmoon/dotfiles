# disable packer.io phone home, see https://www.packer.io/docs/other/environmental-variables.html
export CHECKPOINT_DISABLE=yes

alias pb='packer-io build'
