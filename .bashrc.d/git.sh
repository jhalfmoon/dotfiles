# git completion must be sourced explicitely first, to get access to __git_complete
FILE=/usr/share/bash-completion/completions/git
if [[ -e $FILE ]] ; then
    source $FILE
else
    echo "ERROR: $FILE not found in ${BASH_SOURCE[0]}"
    return 1
fi

# Install autocompletion for aliases
gitshort() {
    local SHORT=$1 ; shift
    alias $SHORT="git $*"
    __git_complete $SHORT _git_$1
}

gitshort gc checkout
gitshort gcb checkout -b
gitshort gm merge
gitshort gd diff
gitshort gdh diff HEAD~
gitshort gdhn diff --name-only HEAD~
gitshort gdc diff --cached
gitshort gdn diff --name-only
gitshort gdcn diff --cached --name-only
gitshort gr rebase
gitshort gri rebase -i
gitshort gb branch

# note: this gitshort is just to activate tab completion, the alias gh gets redefined below
gitshort gh log

gg() {
    git commit -am "$*"
    git push
}

# Update all git repositories recursively in the current path
gup(){
    # Alternatvely: Only update git repos directly located in current folder.
    # local DIRS=$(find -maxdepth 1 -type d -name .git)
    local DIRS=$(find -type d -name .git)
    local TOTAL=$(echo $DIRS | wc -w)
    echo "Updating $TOTAL folders..."
    sleep 1
    local COUNT=1
    for DIR in $DIRS ; do
        DIR=$(readlink -f $DIR/..)
        echo "#=== Updating $COUNT/$TOTAL - $DIR"
        COUNT=$((COUNT+1))
        cd $DIR
        git fetch --all
        # https://stackoverflow.com/questions/292357/what-is-the-difference-between-git-pull-and-git-fetch
        git merge FETCH_HEAD
        cd - > /dev/null
    done
}

alias gi='git init'
alias ga='git add'
alias gbr='git branch --remote'
alias gbl='git blame'
alias gs='git status'
alias gca='git commit --amend'
alias gcl='git clone'
# show all changes for a given file
alias ghf='gh --color --follow --patch -- '
# show summary of history
alias gh='git log --pretty=format:"%h %ad | %s%d [%an]" --graph --date=short --decorate'
alias ghh='git log --pretty=format:"%h %ad | %s%d [%an]" --graph --date=short --decorate | head -n10'

# remap the github cmdline to ghh
#alias ghh=/usr/bin/gh

alias gl='git log'
alias gp='git pull'
alias gpp='git push'
alias gpf='git push -f'
alias gpu='git push -u origin HEAD'
alias grp='git rev-parse'
alias gt='git tag'
alias gst='git stash'
alias gstp='git stash pop'
# used for mindless triggering of webhooks
alias ggg='date > bump ; ga -f bump ; gcm bump $(date +%Y%m%d-%H%M) ; gpp'
# long alias to prevent accidental invocation
alias gdeleteremote='git push origin --delete'
# When did a certain string or line number change?
# https://stackoverflow.com/questions/8435343/retrieve-the-commit-log-for-a-specific-line-in-a-file
# https://stackoverflow.com/questions/9935379/git-show-all-of-the-various-changes-to-a-single-line-in-a-specified-file-over-t
# https://git-scm.com/docs/git-log#Documentation/git-log.txt--Lltstartgtltendgtltfilegt
alias gll='git log -uL'
alias gls='git log -uG'

# remove all non-commited items
alias gclean='git clean -fxd'

gcm() {
    if [[ -n $* ]] ; then
        git commit -m "$*"
    else
        git commit
    fi
}

gprune() {
    echo -n 'Prune non-tracking branches? YES to confirm: '
    read REPLY
    if [[ $REPLY == 'YES' ]] ; then
        git remote prune origin
    else
        echo 'Cancelled.'
    fi
}

gprune2() {
    local ARG
    [[ $1 == '-f' ]] && ARG='-D' || ARG='-d'
    echo -n 'Remove all local branches not on remote (use -f to force delete)? YESPLEASE to confirm: '
    read REPLY
    if [[ $REPLY == 'YESPLEASE' ]] ; then
        git branch -vv | grep ': gone]'|  grep -v "\*" | awk '{ print $1; }' | xargs -r git branch $ARG
    else
        echo 'Cancelled.'
    fi
}

