#!/bin/bash
# ~/.bashrc: executed by bash(1) for non-login shells.

# If not running interactively, don't do anything.
# This should be at the top of your .bashrc, but in case it isn't, it's here
# to prevent future headaches. If you fail to do this, then things like trying
# to scp anything to this node might fail in weird ways.
[[ $- != *i* ]] && return

