#=== z

BASE=$HOME/.bin/z
if [[ ! -d $BASE ]] ; then
    mkdir -p $BASE
    git clone https://github.com/rupa/z $BASE
fi

source $BASE/z.sh
unalias z 2> /dev/null

z() {
    [ $# -gt 0 ] && _z "$*" && return
    cd "$(_z -l 2>&1 | fzf --height 40% --nth 2.. --reverse --inline-info +s --tac --query "${*##-* }" | sed 's/^[0-9,.]* *//')"
}

#===

