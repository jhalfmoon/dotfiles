# Make tmux do 256 colors
# https://github.com/tmux/tmux/wiki/FAQ
# https://www.reddit.com/r/tmux/comments/mesrci/tmux_2_doesnt_seem_to_use_256_colors/
# NOTE: Comment this line out on WSL2 sessions as it will mess up your terminal
export TERM=tmux-256color

# https://wiki.archlinux.org/title/HiDPI#Qt_5
export QT_AUTO_SCREEN_SCALE_FACTOR=1
export QT_ENABLE_HIGHDPI_SCALING=1

# check the window size after each command and, if necessary, update the values of LINES and COLUMNS.
shopt -s checkwinsize direxpand

# expand !! after space
# https://unix.stackexchange.com/questions/147563/how-do-i-repeat-the-last-command-without-using-the-arrow-keys
bind Space:magic-space

# append to history file
shopt -s histappend
export HISTFILE=~/.bash_history
export HISTSIZE=1000000
export HISTFILESIZE=10000000
export HISTTIMEFORMAT="%Y/%m/%d-%H:%M:%S  "
export HISTCONTROL=ignoreboth
export HISTIGNORE=""

export EDITOR=vim

# ssh-host tab completion
if [ -r ~/.ssh/known_hosts ] ; then
    complete -o default -W "$(cat ~/.ssh/known_hosts | cut -f1 -d' ' | sed -e s/,.*//g | cut -d[ -f2 | cut -d] -f1 | sort | uniq)" ssh
fi

# NOTE: The ENV variable used in my PROMPT_COMMAND variable to set the console title.
#       For this to work in tmux, the following must be set: "tmux set-option -g set-titles on" .                                                                               #       Also, when using Konsole, it must be configured with %w in the title.
export ENV='unknown'
[ -f /etc/.env ] && export ENV=$(cat /etc/.env)
[ -f $HOME/.env ] && export ENV=$(cat $HOME/.env)

# Activate my personal git prompt tool
if [[ -e $HOME/.bin/yabagi-prompt.sh ]] ; then
    source $HOME/.bin/yabagi-prompt.sh
else
# Colorize prompt for non-root and root usage
    col_r="\[\033[38;5;1m\]"
    col_g="\[\033[38;5;2m\]"
    col_b="\[\033[38;5;4m\]"
    col_rst="\[\033[0m\]"
    if [ $(whoami) == 'root' ] ; then
        export PS1="${col_r}\u@\h ${col_b}\w ${col_r}\\$ ${col_rst}"
    else
        export PS1="${col_g}\u@\h ${col_b}\w \\$ ${col_rst}"
    fi
    #PROMPT_COMMAND="$PROMPT_COMMAND;"'echo -ne "\[\033]0;\]${USER}@${HOSTNAME}: ${PWD/$HOME/~}   (env:${ENV:-<none>}) \[\007\]"'
    #PROMPT_COMMAND=$(echo "$PROMPT_COMMAND" | sed -E 's/ *;[ ;]+/;/g')
fi

# enable color support of ls and also add handy aliases
if [ "$TERM" != "dumb" ]; then
   [ -s ~/.dircolors ] && eval "`dircolors -b`"
fi

# highlight broken links in red
export LS_COLORS="$LS_COLORS:mi=37;41"

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(lesspipe)"

# unset this nasty variable, that konsole tends to set
unset COLORFGBG

# NOTE: On quite a few terminals CTRL-s stops the output of your session and it will
# appear to be frozen.  If that happens, use CTRL-q to unfreeze it. You may want to
# remove CTRL+s mapping from your terminal altogether:
stty stop ""

if [ "$DISPLAY" != '' ] ; then
    export INTEL_BATCH=1
    if which xset > /dev/null 2>&1 ; then
        #setxkbmap us -variant colemak
        #setxkbmap -option "caps:backspace"
        #
        # disable annoying audible BEEP, if xset is installed
        xset -b
        # and set key repeat on caps lock key
        xset r 66
        xset r rate 190 45
    fi
    #gsettings set org.gnome.desktop.peripherals.keyboard repeat-interval 20
    #gsettings set org.gnome.desktop.peripherals.keyboard delay 200
#    if which xrandr > /dev/null 2>&1 ; then
#        xrandr --output LVDS1 --brightness 1.0 --gamma 1:1:1.3
#    fi
fi

# Display uptime and kernel info
echo ""
UPTIME=$(uptime)
UP=$(echo $UPTIME | sed -n 's/^.*up \([^,]\+\).*$/\1/p')
LOAD=$(echo $UPTIME | cut -d: -f5 | cut -d' ' -f3 | sed 's/,$//g')
KERNEL=$(uname -r)
USERCOUNT=$(who | cut -d' ' -f 1 | sort | uniq | wc -l)
echo "Kernel $KERNEL  Up $UP  Load $LOAD  Users $USERCOUNT"
set | grep '^cpuinfo\s' 2> /dev/null > /dev/null && cpuinfo

set | grep '^nicinfo\s' &> /dev/null && nicinfo

# Show charge of wireless mice and keyboards
if (which upower &> /dev/null) ; then
    upower -e | grep 'mouse_dev\|keyboard_dev' | while read DEV; do
        INFO=$(upower -i $DEV)
        NAME=$(echo "$INFO" | grep 'model:' | sed 's/.*: \+//g')
        CHARGE=$(echo "$INFO" | grep -v ignored | grep percent | awk '{print $2}' | tr -d '%')
        if   [[ $CHARGE -lt 20 ]] ; then COLOR="\033[1;31m"
        elif [[ $CHARGE -lt 50 ]] ; then COLOR="\033[1;33m"
        else COLOR="\033[1;32m"
        fi
        echo -e "$NAME : ${COLOR}${CHARGE}%\033[0m"
    done
fi

if ! (which keychain &> /dev/null) ; then
    echo "INFO: Keychain is not installed. No ssh keys will be added."
else
    source $HOME/.keychain/*-sh 2> /dev/null
fi

KEYROOT="/1/store/keys/ssh"
#addkey vagrant /1/opslag/projects/packer/support/vagrant.privkey

# Add extra linefeed to prompt to always have a 'clean' / UTF
# Note: This must be done after PS1 is fully customized, so make it the last command in your .bashrc
PS1="\n$PS1"

# Hashicorp stuff: ET no phone home
# https://www.vagrantup.com/docs/other/environmental-variables#vagrant_checkpoint_disable
export CHECKPOINT_DISABLE=true

