
# no proxy
alias np="unset http_proxy https_proxy HTTP_PROXY HTTPS_PROXY"

ht() {
    helm template --debug --dry-run . $@ 2>&1 | less -RSXF +G
}

# helm dry-run in a loop
htt() {
    OLDSUM=''
    while true; do
        OUTPUT="$(helm template --debug --dry-run . $@ 2>&1)"
        NEWSUM=$(echo "$OUTPUT" | md5sum)
        if [[ $OLDSUM != $NEWSUM ]] ; then
            OLDSUM=$NEWSUM
            clear
            date
            echo "$OUTPUT"
        fi
        sleep 1
    done
}

#=== k8s =============================================================

# Gather configs into environment variable
KUBECONFIG=$(echo $(ls $HOME/.kube/conf.d/*.conf 2> /dev/null) | tr ' ' ':')

# If configs were found and ~/.kube/config exists, then prepend it to $KUBECONFIG
# TODO: It's better not to use ~/.kube/config at all, because it screws you up when
#       working in multiple windows with different clusters. But for now I'll use it.
FILE="$HOME/.kube/config"
[[ -e "$FILE" ]] && [[ -n "$KUBECONFIG" ]] && export KUBECONFIG=$FILE:${KUBECONFIG:-}

# activate autocompletion
if (which kubectl &> /dev/null) ; then
    source <(kubectl completion bash)
    complete -F __start_kubectl k
    alias k="kubectl"
fi

# info about ns, context, control plane addr / cluster reachability
ki() {
    local IP
    if ! (which kubens &>/dev/null) ; then
        echo "ERROR: kubens not found. Please install it."
        return 1
    else
        if ! (which kubectx &>/dev/null) ; then
            echo "ERROR: kubectx not found. Please install it."
            return 1
        else
            IP=$(timeout 3 kubectl cluster-info | grep 'control plane' | grep -o 'http.*' || echo 'Cluster not reachtable')
            echo "$(kubens -c) @ $(kubectx -c)    ($IP)"
        fi
    fi
}

# kubectl watch - watch current or a given namespace, sort of like a 'get all++'
kw() {
    while true ; do
        FOO=$(date ; kubectl get po,svc,ing,deploy,rs,sts,hpa,cm,secrets,netpol $@)
        clear
        echo "$FOO"
        sleep 1
    done
}

# kubectl test - test a given image
kt() {
    IMAGE=$1
    shift
    OTHER="$@"
    kubectl run temp-pod --rm --restart=Never -it --image=$IMAGE -- ${OTHER:-sh}
}

# Internal support function used by several aliases listed below.
# It parses arguments, uses them to get fetch, filter and return a list of pods in a given kubernetes cluster.
_getPod() {
    local OPTIND ARGS VAR1 VAR2 VAR3
    local SORT="--sort-by=.metadata.name"

    # default to using current namespace
    NAMESPACE=$(kubectl config view --minify -o jsonpath='{..namespace}') || return 1
    [[ -z $NAMESPACE ]] && NAMESPACE=default

    while getopts "An:w" options; do
        case $options in
            A)  ARGS="$ARGS -A"
                NAMESPACE=''
                ;;
            n)  NAMESPACE=$OPTARG
                ;;
            w)  ARGS="$ARGS -o wide"
                # when gathering all pods prom all nodes, then sort by node name instead of pod name
                SORT="--sort-by=.spec.nodeName"
                ;;
        esac
    done
    shift $((OPTIND-1))
    [[ -n $NAMESPACE ]] && ARGS="$ARGS -n $NAMESPACE"

    # check that given namespace exists
    kubectl get namespace $NAMESPACE > /dev/null || return 1

    # Remaining parameters are regarded as search strings and pod/ is removed from them
    QUERY="$(echo $@ | sed 's/pod\///g')"
    #read NAMESPACE POD < <(kubectl get pods $ARGS --no-headers -o custom-columns=NAMESPACE:.metadata.namespace,NAME:.metadata.name | sort -k1 | fzf ${QUERY:+-q "$QUERY"} -i -e -0 -1)
    read VAR1 VAR2 VAR3 < <(kubectl get pods $ARGS --no-headers $SORT | fzf ${QUERY:+-q "$QUERY"} -i -e -0 -1)
    if [[ -z $NAMESPACE ]] ; then
        NAMESPACE=$VAR1
        POD=$VAR2
    else
        POD=$VAR1
    fi

    [[ $? -ne 0 ]] && return 1
    CONTAINERS="$(kubectl get pods $POD -n $NAMESPACE -o jsonpath='{.spec.containers[*].name}{"\n"}' | tr ' ' '\n')"
    read CONTAINER < <(echo "$CONTAINERS" | fzf -i -e -0 -1)
    # export variable that can be used to re-use the out
    LAST="$POD -c $CONTAINER"
    # trick to report exit status of fzf to caller of this function
    return $?
}

# Internal support function used by several aliases listed below.
# It parses arguments, uses them to get fetch, filter and return a list of objects in a given kubernetes cluster.
_getObject() {
    local OPTIND ARGS NAMESPACED STATUS VAR1 VAR2 VAR3
    NAMESPACED=true

    # default to using current namespace
    NAMESPACE=$(kubectl config view --minify -o jsonpath='{..namespace}') || return 1
    [[ -z $NAMESPACE ]] && NAMESPACE=default

    while getopts "An:N" options; do
        case $options in
        A)  ARGS="$ARGS -A"
            NAMESPACE=''
            ;;
        n)  NAMESPACE=$OPTARG
            ;;
        N)  NAMESPACED="false"
            ;;
        esac
    done
    shift $((OPTIND-1))
    [[ -n $NAMESPACE ]] && ARGS="$ARGS -n $NAMESPACE"

    # Use the first of any remaining parameters as a filter for the ojbject type
    # If the filter for the objects is '.' then treat that as '.*' and don't highlight the object in fzf
    if [[ $# -eq 0 ]] || [[ $1 == '.' ]] ; then
        FILTER='.*'
        shift
    # if filter contains slash, then parse out the object type filter and use it
    elif [[ $1 =~ '/' ]]; then
        FILTER=$(echo $1 | cut -d/ -f1 | cut -d\. -f1)
    else
        FILTER=$1
    fi
    QUERY="$@ "

    # check that given namespace exists
    if [[ $NAMESPACED == 'true' ]] ; then
        kubectl get namespace $NAMESPACE > /dev/null || return 1
    fi

    # NOTE: Kuberhealthy* objects cause 'kubectl get' to fail, so are removed from the list
    RESOURCES=$(kubectl api-resources --namespaced=$NAMESPACED --no-headers | grep -vi "events\|Kuberhealthy\|PodMetrics\|NodeMetrics" | rev | cut -d' ' -f1 | rev | sort | grep -i "$FILTER" | paste -s -d, -)
    # Debug
    echo $RESOURCES
    #
    read VAR1 VAR2 VAR3 < <(kubectl get $ARGS $RESOURCES --no-headers --show-kind --ignore-not-found 2> /dev/null | sort | fzf ${QUERY:+-q "$QUERY"} -i -e -0 -1 )
    STATUS=$?
    if [[ -z $NAMESPACE ]] ; then
        NAMESPACE=$VAR1
        OBJECT=$VAR2
    else
        OBJECT=$VAR1
    fi
    return $STATUS
}

# Note: The following functions can be called with parameters:
#   -A  Search across all namespaces (default: off)
#   -n  specify namespace (default: current namespace)
#   -N  search in non-namespaced (default: namespaced objects)
#   First argument: Filter for object types. Examples:
#   Remainging arguments: Filter to use on selected object types.
#   Examples:
#       kgy . trae foo      - Show all objects in current namespace containing string 'trae' and 'foo'
#       kgy pod trae foo    - Show all object-type-names in current namespace containing 'pod' and names containing string 'trae' and 'foo'
#       kgy -A pod trae foo - Show all object-type-names containing 'pod' and names containing string 'trae' and 'foo'
#       kgy -n bar pod      - Show all pods in namespace bar

# enter pod
ke()   { _getPod $@       && (set -x ; kubectl exec -ti $POD -c $CONTAINER -n $NAMESPACE -- sh -c '( bash || ash || sh )') || return 0 ; }
# show pod log
kl()   { _getPod $@       && (set -x ; kubectl logs $POD -c $CONTAINER -n $NAMESPACE) | less -RXSF || return 0 ; }
# tail pod log
klf()  { _getPod $@       && (set -x ; kubectl logs $POD -c $CONTAINER -n $NAMESPACE -f) || return 0 ; }
# get pod
kgp()  { _getPod $@       && (set -x ; kubectl get -o yaml $POD -c $CONTAINER -n $NAMESPACE -f) || return 0 ; }

# edit namespaced object
ked()  { _getObject $@    && (set -x ; kubectl edit -n $NAMESPACE $OBJECT) || return 0 ; }

# yaml of namespaced objects
kgy()  { _getObject $@    && (set -x ; kubectl get -o yaml -n $NAMESPACE $OBJECT) | less || return 0 ; }
# yaml of non-namespaced objects
kgyy() { _getObject -N $@ && (set -x ; kubectl get -o yaml $OBJECT) | less || return 0 ; }

# describe namespaced objects
kd()   { _getObject $@    && (set -x ; kubectl describe -n $NAMESPACE $OBJECT) | less || return 0 ; }
# describe non-namespaced objects
kdd()  { _getObject -N $@ && (set -x ; kubectl describe $OBJECT) | less || return 0 ; }

#alias kev="kubectl get events --sort-by='.metadata.creationTimestamp'"
alias kev="kubectl get events --sort-by='.metadata.managedFields[0].time'"
alias keva="kubectl get events -Aw"
alias kevb="kubectl logs -f -n kube-system \$(k get pods -n kube-system | grep '^kube-cont' | cut -d' ' -f1)"

alias kg="kubectl get --show-kind"

ITEMS="pod,deploy,svc,rs,sts,hpa,cm,secrets,ing,netpol"
alias kga="kubectl get $ITEMS"
alias kgaw="watch -- 'kubectl get $ITEMS'"

alias kx="kubectx"
alias kn="kubens"
# Requires: aurto add bash-complete-alias ; pacman -Syu bash-complete-alias
# https://github.com/cykerway/complete-alias
complete -F _complete_alias kx
complete -F _complete_alias kn

alias ka="kubectl apply"
alias kaf="kubectl apply -f"
alias kdf="kubectl delete -f"
alias kc="kubectl create"
alias kk="kubectl kustomize"
alias kdel='kubectl delete'

alias kgd="kubectl get deployment"
alias kgc="kubectl get configmap"
alias kgs="kubectl get svc"
alias kgpv="kubectl get pv"
alias kgpvc="kubectl get pvc"
alias kgn="kubectl get ns"
alias kgno="kubectl get nodes"
alias kgnp="kubectl get netpol"
alias kgi="kubectl get ingress"

alias kdn="kubectl describe nodes"

# show all repos
alias hrl='helm repo list'
# show all versions
alias hs='helm search repo -l'
# search version
alias hsv='helm search repo --version'
# search regex
alias hsv='helm search repo -r'

# This function is used by kdump() below
_dump() {
    local NS="$1"
    local PARAM1="$2"
    local PARAM2="$3"
    local RESOURCES=$(kubectl api-resources --no-headers -o name $PARAM1 | grep -vi 'events\|podmetrics\|nodemetrics' | cut -d. -f1 | sort);

    echo === $NS

    # Get list of resources per type
    echo "$RESOURCES" | while read RESOURCE ; do
        # Redirect stderr to catch messages such as:
        # "Error from server (MethodNotAllowed): the server does not allow this method on the requested resource"
        INSTANCES=$(kubectl get $RESOURCE -o name $PARAM2 2>/dev/null)
        # If previous command did not return an error and output is not empty
        if [[ $? -eq 0 ]] && [[ -n $INSTANCES ]] ; then
            echo -n "$RESOURCE "
            echo "$INSTANCES" | while read INSTANCE; do
                TYPE=$(echo $INSTANCE | cut -d'/' -f1)
                NAME=$(echo $INSTANCE | cut -d'/' -f2)
                DIR=$BASE_DIR/$NS/$TYPE &> /dev/null
                mkdir -p $DIR &> /dev/null
                kubectl get $INSTANCE -o yaml 2>/dev/null > $DIR/$NAME
            done
        fi
    done
    # Add extra linefeed
    echo
}

# Dump all resources in one or more namespaces to seperate files.
# When called without parameters, it will dump the current namespace to the default base dir.
# Usage:
#   -a  Dump all namespaces (default: dump only current namespace)
#   -c  Only dump non-namespaced (AKA 'cluster') manifests
#   -d  Base directory used for placing the yaml manifests (default: /tmp/k8s-dump)
#   -n  Namespace to dump (default: Current namespace)
# NOTE: This function is slow and generates about quite some traffic to the k8s API, but
#       it relies only on kubectl and shell tools for processing. It could be made faster
#       by instead of iterating over and fetching each manifest seperately, by fetching
#       the resources in groups and then parsing the resulting block of manifests. But
#       this would mean adding a dependency on jq or probably yq. So that is why that has
#       not been implemented.
kdump() {
    # default output dir
    local BASE_DIR="$HOME/tmp/k8s-dump"
    # default to the current namespace
    local NAMESPACES="$(kubectl config view --minify --output 'jsonpath={..namespace}')"

    local OPTIND OPTARG options
    while getopts "acd:n:" options ; do
        case $options in
        # https://kubernetes.io/docs/reference/kubectl/jsonpath/
        a) NAMESPACES="$(kubectl get ns -o=jsonpath='{.items[*].metadata.name}')"
           ;;
        c) NAMESPACES='cluster'
           ;;
        d) BASE_DIR=$OPTARG
           ;;
        n) NAMESPACES=$OPTARG
           ;;
        esac
    done

    echo "Dumping the following namespace to $BASE_DIR:"
    echo "  $NAMESPACES"
    for NS in $NAMESPACES ; do
        if [[ -e $BASE_DIR/$NS ]] ; then
            echo "WARNING: $BASE_DIR/$NS already exists. Skipping dump of $NS."
        else
            if [[ $NS == 'cluster' ]] ; then
                _dump $NS --namespaced=false
            else
                _dump $NS --namespaced=true --namespace=$NS
            fi
        fi
    done
}

#=== This bit here is more for later reference that for direct use

# Remove finalizers:
# kubectl patch some.crd/crd_name -p '{"metadata":{"finalizers":[]}}' --type=merge

# Get non-namspaced resources based on label
__getObject_by_label() {
    echo === $@
    RESOURCES=$(kubectl api-resources --namespaced=false --no-headers -o name | grep -v "events" | cut -d. -f1  | paste -s -d, -)
    kubectl get $RESOURCES -o name --no-headers --show-kind --ignore-not-found $@ 2> /dev/null
}

# Delete resources based on label
#__getObject_by_label -l app.kubernetes.io/name=cert-manager | sort -r |while read ding ; do kubectl delete $ding ; done
#__getObject_by_label -l app.kubernetes.io/part-of=argocd

#=== crio ============================================================

#    https://kubernetes.io/docs/tasks/debug-application-cluster/crictl/

_cgetContainer() {
    read ID NAME < <(sudo crictl ps -a $@ | sed 1d | sort -k6,7 | fzf -e -i -0 -1 | awk '{print $1,$7}');
    [[ -n $NAME ]] && return 0 || return 1
}

alias cli="sudo crictl images"
alias clc="sudo crictl ps -a"

alias ce="_cgetContainer && (echo \"Entering container \$NAME\" && sudo crictl exec -ti \$ID sh)"
alias ci="_cgetContainer -a && (echo \"Entering container \$NAME\" && sudo crictl inspect \$ID | less -RXSF)"
alias cl="_cgetContainer -a && (echo \"Logs for \$NAME\" && sudo crictl logs \$ID 2>&1 | less -RXSF)"
alias clf="_cgetContainer -a && (echo \"Logs for \$NAME\" && sudo crictl logs -f \$ID 2>&1 )"


#=== Docker ==========================================================

# https://docs.docker.com/reference/cli/docker/

# Tip: To see what docker-ps can output, run: docker ps --format '{{json .}}'
_getContainer() {
    local QUERY
    QUERY=$@
    # Store entire status line of selected container for later use
    LINE="$(docker ps -a | sed 1d | sort -k2 | fzf -e -i -0 -1 ${QUERY:+-q "$QUERY"})"
    # Parse out the containers ID
    read ID FOO < <(echo $LINE | awk '{print $1,$2}')
    # Fetch the containers name, as parsing it from the stored status line is too much trouble with the inconsistent spacing of that line
    read NAME   < <(docker ps -af "id=$ID" --format "{{.Names}}")
    # Return FALSE if no container was found / selected
    [[ -n $ID ]] && return 0 || return 1
}

_getImage() {
    local QUERY
    QUERY=$@
    read NAME TAG ID FOO < <(docker images | sed 1d | sort -k2| fzf -e -i -0 -1 ${QUERY:+-q "$QUERY"} | awk '{print $1,$2,$3}')
    [[ -n $ID ]] && return 0 || return 1
}

de()  { _getContainer $@ && echo -e "$LINE" && docker exec -ti $ID sh -c '(bash || ash || sh )'                    || return 0 ; }
dlf() { _getContainer $@ && echo "Logs for $NAME"           && docker logs -f $ID 2>&1                             || return 0 ; }
dl()  { _getContainer $@ && echo "Logs for $NAME"           && (docker logs $ID 2>&1 | less -R)                    || return 0 ; }
dk()  { _getContainer $@ && echo "Killing container $NAME"  && docker container kill $ID ; docker container rm $ID || return 0 ; }
dic() { _getContainer $@ && echo "Inspect container $NAME"  && (docker inspect $ID | less -RXSF)                   || return 0 ; }
dii() { _getImage     $@ && echo "Inspect image $NAME"      && (docker inspect $ID | less -RXSF)                   || return 0 ; }

alias dlc='docker container list -a'
alias dli='docker images'
alias ddn="docker network prune -f"
alias dda='docker system prune -a -f'

#alias ddc='echo "Stopping containers... " ; docker stop $(docker container list -q) 2>/dev/null ; dclean'
#alias ddi='docker images -q | while read ding ; do docker rmi -f $ding ; done'

alias d='docker'
alias db='docker buildx build'
alias dp='docker pull'
alias dps='docker ps'
alias dpp='docker push'
alias dpr='docker image prune -a'
alias dsi='docker system df ; echo '' ; docker images ; echo '' ; docker container list -a'

alias dtag='docker tag'
alias dr='docker run -d'
alias dri='docker run -it --restart always'
alias drmi='docker rmi -f'
alias dln='docker network ls'

# test-run an image, open a shell and delete the container immediately after exit
# commit last run container
alias dco='docker commit $(dlast)'
# show last run container
alias dlast='docker container list -l -q'
# prune build cache
alias dprune='docker system prune -fa ; docker volume rm $(docker volume ls -qf dangling=true)'

dt() {
    IMAGE=$1
    shift
    if [[ -n $@ ]] ; then
        CMD="--entrypoint $@"
    else
        CMD=''
    fi
    docker run -it --rm -v /tmp:/tmp-host $CMD $IMAGE
}

# stop and remove containers
ddc() {
    LIST=${1:-$(docker container list -qa)}
    for ding in $LIST ; do docker rm -f $ding; done
    dclean
}

# remove all non-running images
ddi() {
    LIST=${1:-$(docker images -q)}
    for ding in $LIST ; do docker rmi -f $ding ; done
}

# docker image dump to tarball
didump() {
    IMAGE=$1
    FILE="${IMAGE//\//_}.tar.zst"
    ID=$(docker create $IMAGE)
    echo "Creating filesystem tarball of docker image.."
    if (docker export $ID | zstd > $FILE) ; then
        echo "Dump created to $FILE."
        ls -alh $FILE
    fi
    docker rm -f $ID
}

dclean() {
    echo "Removing unused image layers and untagged images."
    #CONTAINERS=$(docker container list -qa)
    #[[ -n $CONTAINERS ]] && docker rm $CONTAINERS
    #IMAGES=$(docker images -a | grep "<none>" | awk '$1=="<none>" {print $3}')
    IMAGES=$(docker images -a | grep "<none>" | awk '{print $3}' | sort | uniq)
    [[ -n $IMAGES ]] && docker rmi -f $IMAGES
    docker rmi $(docker images --filter "dangling=true" -q --no-trunc) 2>/dev/null || echo "No dangling images found"
}

# backup / restore image
dsave() { sudo docker save "$1" -o "${1//\//_}.docker-image" ; }

ddump() {
    sudo docker images | tail -n+2 | while read a b c ; do
        echo "Saving $a:$b"
        dsave "$a:$b"
    done
}

dload() { sudo docker load $1 ; }

drestore() {
    ls *.docker-image | while read ding ; do
        echo "Restoring $ding"
        dload $ding
    done
}

