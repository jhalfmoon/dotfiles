OpenVPN howto
#############

jhalfmoon 20240210

Requirements
============

Client
------
The instructions for RDP/VNC are for Windows clients only. I currently have no use for Linux desktop sharing.

* openvpn - https://openvpn.net/community-downloads/
* TightVNC - https://www.tightvnc.com/download.php

When using a windows client, I having chosen TightVNC to be the most convenient VNC client currently available. Note that Windows 10/11 Pro is required to be able to use RDP. When using RDP and/or VNC, ensure that the firewall will allow the incoming connections. Also, ensure that RDP is enabled and that the correct users have rights to use it.

Server
------
It is assumed that the OpenVPN server is a Linux machine.

* openvpn
* realvnc-vnc-viewer
* iptables configured to allow openvpn traffic, ie. tcp and udp 4444
* WAN port forwarding configured for openvpn; TCP and UDP port 4444 for src and dest.

Usage
=====

Server-side::

    ./vpn-gen-certs.sh
    DIR=/path/to/your/vpn-secrets
    mv secrets-new $DIR
    ./vpn.sh $DIR

Ensure that you link or move vpn.sh to a folder in your path.

Openvpn client on windows uses the path C:\Users\USERNAME\OpenVPN\config to store the secrets. Copy the following over to the openvpn client and start the client openvpn connection::

    ca.crt
    server.crt
    client.crt
    client.key
    secret.key

First start the client connection. Then check the logs on the server to see what the client local IP is.

Next edit the file source-me.sh to contain the IP address you just found.

Source the file source-me.sh and test the  RDP or VNC connection to the client desktop.

About VNC
=========

If you want/need to tune the VNC connection, you can find RealVNC client parameters here: https://help.realvnc.com/hc/en-us/articles/360002254618-VNC-Viewer-Parameter-Reference. Notable settings are:

Quality
* custom
* auto
* high
* medium
* low

PreferredEncoding
* JPEG
* Zlib
* ZRLE
* ZRLE2
* TRLE
* Hextile
* Raw
* JRLE

ColorLevel
* rgb111 to use 8 colors
* rgb222 to use 64 colors
* pal8 to use 256 colors
* full to use full color

