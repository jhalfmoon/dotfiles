#!/bin/bash
#
# Wrapper script to start openvpn server

HERE=$(dirname $(readlink -f $0))
DIR=$1

if [[ $# -ne 1 ]] ; then
    echo "ERROR: Expecting a single argument. Please supply path to VPN secrets."
    exit 1
fi

if [[ -d $DIR ]] ; then
    sudo openvpn --cd $DIR --verb 11 --config $HERE/configs/lnx-server.ovpn
else
    echo "ERROR: Directory '$DIR' does not exist."
    exit 1
fi
