# This files contains suggestions for aliases to simplify connecting to a desktop over RDP and VNC.

IP=10.10.0.2
USERNAME=foo
PASSWORD=bar

alias rdp="xfreerdp +fonts +window-drag /w:2000 /h:1200 /audio-mode:1 /drive:myshare,/1/download /clipboard /compression-level:7 /codec-cache:rfx /rfx /frame-ack:5 /network:auto /gfx:avc444 /u:$USERNAME /p:$PASSWORD /v:$IP"

# Alternatively, if you want control over bandwidth usage:
# alias vnc="vncviewer -Scaling fitauto -Quality custom -ColorLevel full -PreferredEncoding ZRLE2 10.10.0.2"
#
alias vnc="vncvncviewer -Scaling fitauto -Quality medium $IP"

