#!/bin/bash -x

HERE=$(dirname $(readlink -f $0))
DIR=$HERE/secrets-new

openssl s_server -CAfile $DIR/ca.crt -cert $DIR/server.crt -key $DIR/server.key -Verify 1

