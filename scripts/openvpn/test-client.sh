#!/bin/bash -x

HERE=$(dirname $(readlink -f $0))
DIR=$HERE/secrets-new

openssl s_client -CAfile $DIR/ca.crt -cert $DIR/client.crt -key $DIR/client.key

