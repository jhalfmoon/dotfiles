#!/bin/bash -x

# jhalfmoon 20240201
# Generate the entire set of keys, certs and secretts needed to
# configure a working openvpn server and client setup.

DIR=secrets-new
DATE=$(date +%Y%m%d-%H%M)
HERE=$(dirname $(readlink -f $0))

DAYS=36500
CURVE=secp521r1
SUBJ="/C=US/ST=foo state/L=foo city/O=foo inc"
CN_BASE=beep

if ! (which openssl &>/dev/null) ; then
    echo "ERROR: openssl not found. Please install it."
    exit 1
elif ! (which openvpn &>/dev/null) ; then
    echo "ERROR: openvpn not found. Please install it."
    exit 1
fi

[[ -d $DIR ]] && mv $DIR ${DIR}.${DATE}.old
mkdir -p $DIR
cd $DIR

# creat ca
openssl req     \
    -x509       \
    -nodes      \
    -days $DAYS \
    -keyout ca.key \
    -out    ca.crt \
    -newkey ec:<(openssl ecparam -name "$CURVE") \
    -subj "${SUBJ}/CN=${CN_BASE}-ca"

# create server req
openssl req \
    -new    \
    -nodes  \
    -out    server.csr \
    -keyout server.key \
    -newkey ec:<(openssl ecparam -name "$CURVE") \
    -subj "${SUBJ}/CN=${CN_BASE}-server"

# create client req
openssl req \
    -new    \
    -nodes  \
    -out    client.csr \
    -keyout client.key \
    -newkey ec:<(openssl ecparam -name "$CURVE") \
    -subj "${SUBJ}/CN=${CN_BASE}-client"

# sign server csr
openssl x509        \
    -req            \
    -days $DAYS     \
    -CA    ca.crt   \
    -CAkey ca.key   \
    -in    server.csr \
    -out   server.crt

# sign client csr
openssl x509        \
    -req            \
    -days $DAYS     \
    -CA    ca.crt   \
    -CAkey ca.key   \
    -in    client.csr \
    -out   client.crt

openssl genpkey -genparam -algorithm DH -out dh.pem

openvpn --genkey secret secret.key

rm *.csr *.srl &>/dev/null

cat <<EOF
To test this setup, in one shell, run the following:
  openssl s_server -CAfile $DIR/ca.crt -cert $DIR/server.crt -key $DIR/server.key -Verify 1
In another shell, run the following:
  openssl s_client -CAfile $DIR/ca.crt -cert $DIR/client.crt -key $DIR/client.key
Once the negotiation is complete, any line you type is sent over to the other side.

Alternatively you could use the test scripts found in the same folder as this script.
EOF
