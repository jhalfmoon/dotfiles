#!/bin/bash -e

# 20240112 jhalfmoon
# Generate a ca and client/server cert using openssl.
# Keys are generated using ECDSA.

# Break glass in case of emergency...
#
# alias crt-info='openssl x509 -text -in'
# alias csr-info='openssl req -text -noout -in'
#
# alias crt-mod='openssl x509 -modulus -noout -in'
# alias key-mod='openssl rsa -modulus -noout -in'
# alias csr-mod='openssl req -modulus -noout -in'
#
# Create csr from cert
#   openssl x509 -x509toreq -in fd.crt -out fd.csr -signkey fd.key
# Dump certificate chain from site
#   echo | openssl s_client -showcerts -connect $HOST:443 -servername $VHOST 2>/dev/null | sed -n '/BEGIN.*-/,/END.*-/p'
# Dump only site certificate
#   echo | openssl s_client -connect $HOST:443 -servername $VHOST 2>/dev/null | sed -n '/BEGIN.*-/,/END.*-/p'

HERE=$(dirname $(readlink -f $0))

CERT_DIR=${CERT_DIR:-/tmp/certs}
CA_BASE_NAME=myca
DAYS=1000
DH_SIZE=2048

# The config file determines the characteristics of the generated certificate. Using multi generally yields satisfactory results.
CONFIGFILE=$HERE/multi.cnf

gen_key() {
    # If the key file exists, then refuse to do anything, as we never want to overwrite a key file.
    local KEYFILE=$1-key.pem
    if [[ -e $KEYFILE ]] ; then
        echo "The file $KEYFILE exists; Exiting."
        return 1
    else
        echo "Generating certificate for $2"
        [[ -n $3 ]] && PARMS="-addext subjectAltName=$3" || PARMS=''
        openssl genpkey -algorithm EC -pkeyopt ec_paramgen_curve:P-256 -out $KEYFILE
    fi
}

# Note: A self signed cert is always a CA cert. You can use it as a CA cert to sign server/client certs or you could directly use it as a server/client cert.
selfsign() {
    if gen_key $@ ; then
        openssl req -x509 -sha256 -noenc -days $DAYS -subj "/CN=$2" -key $1-key.pem -out $1-crt.pem ${PARMS[@]}
    fi
}

sign() {
    if gen_key $@ ; then
        openssl req -new -sha256 -noenc -subj "/CN=$2" -key $1-key.pem -out $1-csr.pem ${PARMS[@]}
        openssl x509 -req -copy_extensions copyall -days $DAYS -extfile $CONFIGFILE -CA ${CA_BASE_NAME}-crt.pem -CAkey ${CA_BASE_NAME}-key.pem -in $1-csr.pem -out $1-crt.pem
    fi
}

# A Diffie Hellman file is not a part of certificates, but used by applications that do TLS termination.
gen_dh() {
    local DH_FILE=dhparams.pem
    if [[ -e $DH_FILE ]] ; then
        echo "The file $DH_FILE exists; Exiting."
        return 1
    fi
    echo "Generating dh file..."
    openssl dhparam -out $DH_FILE $DH_SIZE
}

# Install CA certificate into OS
install_ca() {
    [[ $# -ne 2 ]] && fail "install_ca requires 2 parameters: CERT_DIR and CERT_FILE; Exiting."
    local DIR=$1
    local FILE=$2
    [[ -e $DIR/$FILE ]] || fail "$DIR/$FILE does not exist; Exiting."
    # https://www.freedesktop.org/software/systemd/man/latest/os-release.html
    # https://www.linux.org/docs/man5/os-release.html
    local OS_FILE=/etc/os-release
    if [[ ! -e $OS_FILE ]] ; then
        fail "$OS_FILE not found."
    else
        source $OS_FILE
        case $ID in
            "arch")
                # https://wiki.archlinux.org/title/User:Grawity/Adding_a_trusted_CA_certificate
                sudo trust anchor --store $DIR/$FILE
                ;;
            "fedora")
                # sudo dnf install -y ca-certificates
                sudo cp $DIR/$FILE /etc/pki/ca-trust/source/anchors/
                sudo update-ca-trust || fail "update-ca-trust failed. Is ca-certificates installed?"
                ;;
            "ubuntu")
                # sudo apt-get install -y ca-certificates
                sudo cp $DIR/$FILE /usr/local/share/ca-certificates/
                grep -q $FILE /etc/ca-certificates.conf >/dev/null || echo $FILE | sudo tee -a /etc/ca-certificates.conf >/dev/null
                sudo update-ca-certificates --fresh || fail "update-ca-certifices failed. Is ca-certificates installed?"
                ;;
            *)
                fail "Unknown distro. Exiting without adding ca to trust store."
                ;;
        esac
    fi
}

fail() {
    echo "ERROR: $1"
    exit 1
}

# Do a sanity check
if ! (which openssl &>/dev/null) ; then
    fail "Openssl not installed."
elif [[ -z $CERT_DIR ]] ; then
    fail "CERT_DIR is not set."
elif [[ ! -d $CERT_DIR ]] ; then
    mkdir $CERT_DIR
fi

cd $CERT_DIR

# Example usage:

#<self>sign  $FILENAME_PREFIX   $SUBJECT   $SUBJECT_ALT
# Generate CA
selfsign    $CA_BASE_NAME   "self-signed.root.ca.timestamp.$(date +%s)"
# Generate client or server cert, depending on config used
SITE=proxy.local
sign        $SITE           $SITE       IP:127.0.0.1,DNS:localhost,DNS:www.$SITE
#install_ca  $CERT_DIR ${CA_BASE_NAME}-crt.pem
gen_dh

echo "Certifcates are located in $CERT_DIR"
ls -alh $CERT_DIR
