#!/bin/bash -e

gen_key() {
    openssl genpkey -algorithm EC -pkeyopt ec_paramgen_curve:P-256 -out $1-key.pem
}

selfsign() {
    gen_key $1
    [[ -n $3 ]] && PARMS="-addext subjectAltName=$3" || PARMS=''
    openssl req -x509 -sha256 -noenc -days $DAYS -subj "/CN=$2" -key $1-key.pem -out $1-crt.pem ${PARMS[@]}
}

sign() {
    gen_key $1
    [[ -n $3 ]] && PARMS="-addext subjectAltName=$3" || PARMS=''
    openssl req -new -sha256 -noenc -subj "/CN=$2" -key $1-key.pem -out $1-csr.pem ${PARMS[@]}
    openssl x509 -req -copy_extensions copyall -days $DAYS -extfile $CONFIGFILE -CA $CA_NAME-crt.pem -CAkey $CA_NAME-key.pem -in $1-csr.pem -out $1-crt.pem
}

gen_dh() {
    openssl dhparam -out dhparams.pem 2048
}

HERE=$(dirname $(readlink -f $0))
CONFIGFILE=$HERE/multi.cnf

DAYS=1000
CA_NAME=myca
DIR=/tmp/certs

[[ -d $DIR ]] && echo "ERROR: $DIR already exist. Exiting" && exit 1
mkdir $DIR
cd $DIR

selfsign   $CA_NAME      "self-signed.root.ca.timestamp.$(date +%s)"
sign       example.local  example.local                                  DNS:test.one.local,IP:1.1.1.1
gen_dh
