# A nice, clean, readable Makefile template.
# Building a Makefile project becomes very easy and non-time-consuming using this template.
# An example Makefile, that uses this template, can be found along with this file.
# 20121201 jhalfmoon

#=== Files and path definitions ================

OUTPUT_DIR = build/
IMAGE_PATH = $(OUTPUT_DIR)firmware
LD_SCRIPT_STATIC = $(BUILDSUPPORT_DIR)arm-cortex.ld
LD_SCRIPT_GENERATED = $(OUTPUT_DIR)memory-map.ld

ifndef LD_SCRIPT
LD_SCRIPT = $(LD_SCRIPT_GENERATED)
endif

# This is only used when LD_SCRIPT is not given in Makefile
ifndef IMAGE_OFFSET
IMAGE_OFFSET=0
endif

ifndef DONGLE
DONGLE = undefined
endif
FLASH_SCRIPT = $(BUILDSUPPORT_DIR)flash.sh -d $(DONGLE)

#=== Tooling paths =============================

#TOOLCHAIN = /home/beep/.platformio/packages/toolchain-gccarmnoneeabi/bin/arm-none-eabi-
TOOLCHAIN = arm-none-eabi-
CC = $(TOOLCHAIN)gcc
AS = $(TOOLCHAIN)gcc
LD = $(TOOLCHAIN)gcc
OC = $(TOOLCHAIN)objcopy
OD = $(TOOLCHAIN)objdump
SIZE = $(TOOLCHAIN)size

#=== Build-time parameters =======================

ifndef (,$(findstring($(TARGET),STM32F1)))
	CORE = cortex-m3
	ARCH = armv7-m
endif

# On VPATH: Make searches the current directory and then directories listed in VPATH for given files.
INC_FLAGS = $(patsubst %,-I%,$(VPATH))
LIB_FLAGS = $(patsubst %,-L%,$(LIB_FILES))
DEFINE_FLAGS = $(patsubst %,-D%,$(DEFINES))

# Add output directory prefix to object filenames
OBJS = $(addsuffix .o,$(addprefix $(OUTPUT_DIR),$(notdir $(SRC_FILES))))
DEPS = $(OBJS:.o=.d)

# Create list of .h files to use as dependency
#A_DEPS = $(foreach dir,$(INC_DIR),$(wildcard $(dir)*.h))

ifeq ($(CORE),'cortex-m4')
COMMON_FLAGS += 		\
	-mfpu=fpv4-sp-d16	\
	-mfloat-abi=hard
else
COMMON_FLAGS += 		\
	-mfloat-abi=soft
endif

COMMON_FLAGS +=     	\
	$(DEFINE_FLAGS)		\
	$(INC_FLAGS) 		\
	-march=$(ARCH) 		\
	-mcpu=$(CORE) 		\
	-mthumb				\
	-Wall				\
	-g3 				\
 	-gdwarf-4			\
	-fverbose-asm		\
	-nostdlib			\
	-fmessage-length=0 	\
	-fno-common			\
	-ffreestanding		\
	-fdata-sections 	\
	-ffunction-sections

ifeq ($(DEBUG),1)
	COMMON_FLAGS +=		\
		-fno-builtin 	\
		-O0
else
	COMMON_FLAGS += 	\
		-Os
endif

CPP_FLAGS = $(COMMON_FLAGS)
CC_FLAGS = $(COMMON_FLAGS) -Wstrict-prototypes
AS_FLAGS = $(COMMON_FLAGS) -x assembler-with-cpp
LD_FLAGS =	-T$(LD_SCRIPT)		\
			$(LIB_FLAGS)		\
			-march=$(ARCH) 		\
			-mcpu=$(CORE) 		\
			--specs=nano.specs	\
			--specs=nosys.specs	\
			-Wl,-Map=$(IMAGE_PATH).map,-X,--cref,--relax,--no-warn-mismatch,--gc-sections

#=== Build recipes =================================

noflash : all
run : all flash_and_run
halt : all flash_and_halt
force : all force_flash
all : debug_makefile $(OUTPUT_DIR) $(IMAGE_PATH).elf

flash_and_run : $(IMAGE_PATH).elf
	@$(FLASH_SCRIPT) -r

flash_and_halt : $(IMAGE_PATH).elf
	@$(FLASH_SCRIPT)

force_flash : $(IMAGE_PATH).elf
# force restart of gdb-server and force flashing of the image
	@$(FLASH_SCRIPT) -s -f

debug_makefile :
ifeq ($(VERBOSE),1)
	@echo "=== Makefile debug info ======="
	@echo ""
	@echo Objects: $(OBJS)
	@echo ""
	@echo CC_FLAGS: $(CC_FLAGS)
	@echo ""
	@echo AS_FLAGS: $(AS_FLAGS)
	@echo ""
	@echo LD_FLAGS: $(LD_FLAGS)
	@echo ""
	@echo "==============================="
	@echo ""
endif

$(OUTPUT_DIR):
	@echo $(PATH)
	$(shell mkdir -p $(OUTPUT_DIR) >/dev/null)

$(OUTPUT_DIR)%.o : %.c
ifeq ($(VERBOSE),1)
	$(CC) $(CC_FLAGS) -c $< -o $@
else
	@echo "  CC    $<"
	@$(CC) $(CC_FLAGS) -c $< -o $@ >/dev/null
endif

$(OUTPUT_DIR)%.o : %.cpp
ifeq ($(VERBOSE),1)
	$(CC) $(CPP_FLAGS) -c $< -o $@
else
	@echo "  CPP   $<"
	@$(CC) $(CPP_FLAGS) -c $< -o $@ >/dev/null
endif

$(OUTPUT_DIR)%.o : %.s
ifeq ($(VERBOSE),1)
	$(AS) $(AS_FLAGS) -c $< -o $@
else
	@echo "  AS    $<"
	@$(AS) $(AS_FLAGS) -c $< -o $@ >/dev/null
endif

$(IMAGE_PATH).elf : $(OBJS)
# insert memory addresses and sizes into linker script
ifeq ($(TARGET),something_with_lpc_in_it)
	@echo -e "_image_offset = $(IMAGE_OFFSET);"\
		"MEMORY {"\
		"  FLASH(rx): ORIGIN = 0x00000000, LENGTH = $(FLASH_SIZE)"\
		"  SRAM(rwx): ORIGIN = 0x10000000, LENGTH = $(SRAM_SIZE)}"\
		"INCLUDE $(LD_SCRIPT_STATIC)" > $(LD_SCRIPT_GENERATED)
else
	@echo -e "_image_offset = $(IMAGE_OFFSET);"\
		"MEMORY {"\
		"  FLASH(rx): ORIGIN = 0x08000000, LENGTH = $(FLASH_SIZE)"\
		"  SRAM(rwx): ORIGIN = 0x20000000, LENGTH = $(SRAM_SIZE)}"\
		"INCLUDE $(LD_SCRIPT_STATIC)" > $(LD_SCRIPT_GENERATED)
endif
ifeq ($(VERBOSE),1)
	$(LD) $(LD_FLAGS) $(LIBS) $(OBJS) -o $(IMAGE_PATH).elf
else
	@echo "  LD     $<"
	@$(LD) $(LD_FLAGS) $(LIBS) $(OBJS) -o $(IMAGE_PATH).elf >/dev/null
endif
	@echo ' '
	@$(OC) $(OC_FLAGS) -O binary $(IMAGE_PATH).elf $(IMAGE_PATH).bin
	@$(OC) $(OC_FLAGS) -O ihex $(IMAGE_PATH).elf $(IMAGE_PATH).hex
	@$(OC) $(OC_FLAGS) -R .ramtext $(IMAGE_PATH).elf $(IMAGE_PATH).elf
	@$(OC) $(OC_FLAGS) -O binary -R .ramtext $(IMAGE_PATH).elf $(IMAGE_PATH).bin
    # Relocate from FLASH to RAM, for debugging code in RAM
#	@$(OC) $(OC_FLAGS) -j .ramtext $(IMAGE_PATH).elf $(IMAGE_PATH)_ram.elf
#	@$(OC) $(OC_FLAGS) -O binary -j .ramtext $(IMAGE_PATH).elf $(IMAGE_PATH)_ram.bin
	# Create memory dump
	@$(OD) -x --syms $(IMAGE_PATH).elf > $(IMAGE_PATH).dmp
	# Creating extended listing, useful for analyzing generated code
	@$(OD) -S $(IMAGE_PATH).elf > $(IMAGE_PATH).lss
	@echo 'Size of modules:'
	@$(SIZE) -B -t --common $(OBJS) $(USER_OBJS)
	@echo ' '
	@echo 'Size of target .elf file:'
	@$(SIZE) -B $(IMAGE_PATH).elf
	@echo ' '

clean:
	@echo 'Removing all generated files from output directory: $(OUTPUT_DIR)'
ifeq ($(VERBOSE),1)
	rm -rf $(OUTPUT_DIR)/*
else
	@rm -rf $(OUTPUT_DIR)/* > /dev/null
endif

#=== Dependencies ====================================

.PHONY: all clean reset flash ram load startopenocd debug gdb connect
.SECONDARY:
-include $(DEPS)


