#!/bin/bash -u

# 20191211 kvvliet
# A script to simplify the debugging of embedded code.
#   - Automatically detects the devicename of a connected Black Magic Probe
#   - Detects if the given image needs to be reflashed and does so if needed
#   - Starts an interactive gdb session with the application either running or halted
#   - By default, .gdbinit is ignored and sane defaults are given

GDB="arm-none-eabi-gdb"
#GDB="/home/beep/.platformio/packages/toolchain-gccarmnoneeabi/bin/arm-none-eabi-gdb"

# Set default path to source root, can be overriden on the command line
SOURCEDIR=$(pwd)
OPENOCD_PROBE=localhost:3333

IMAGE=''
PROBE=''
RUN_FLAG=0
FORCE_FLASH_FLAG=0
SKIP_FLASH_FLAG=0
DRY_RUN_FLAG=0
INTERACTIVE_FLAG=1
DETECT_ONLY=0

GDB_EXTRA_CMD=''
GDB_PRE_CONFIG=''
GDB_POST_CONFIG=''
GDB_INIT_CMD="-ex 'set history save on' -ex 'set history filename ~/.gdb_history' -ex 'set confirm off' -ex 'set mem inaccessible-by-default off' -ex 'set mi-async on' -ex 'set disassemble-next-line on'"

#=================================================

usage() {
    SELF=$(basename $(readlink -f $0))
    cat <<EOF

$SELF - A script to make flashing and debugging MCU's easy.

Flashing is only done if the given image file differs from that on the flash memory.
A debug session is started directly afterwards.

Usage: $SELF <-i path> [ options ]
-i <path>       path to elf image
-p <device>     override autodect and manually set the probe to use <device>
-s <path>       path to root of sour directory, not needed if called from it
-c <file>       add config file(s) to use pre-connect
-C <file>       add config file(s) to use post-connect
-x <cmd>        extra gdb command to startup (can be used multiple times)
-b <address>    set breakpoint
-r              run the code and remain in gdb (default: halt)
-e              exit after run, do not start interactive session
-f              force flashing, ignoring image currently on flash
-n              do not flash (only useful for testing this script)
-h              help

Examples:

Flash image only and start interactive session
  flash.sh -i build/firmware.elf
Set source root dir for gdb, flash image, set breakpoint at Reset_Handler, run the code
  flash.sh -r -b Reset_Handler -s ~/projects/bootloader/f1/ -i ~/projects/bootloader/f1/build/hid_bootloader.elf

Notes:
 -  $HOME/.gdbinit is never used unless explicitely given with -c or -C.
- If you receive the warning 'No such file or directory', then either you are not located in the buildroot folder and / or you have not supplied the -s parameter correctly. It means the sources were not found.
- If you get "Function "main" not defined.", it is probably because you ran 'start', which tries to set a breakpoint at main, which fails is main() is not defined.

EOF
}

fail() {
    echo "ERROR: $1" >&2
    exit 1
}

# Run gdb to determine if the target needs to be re-flashed
FlashNeeded() {
    GDB_OUTPUT="$(eval -- $GDB_BASE_CMD $GDB_CONNECT_CMD --batch -ex 'compare-sections' 2>&1)"
    GDB_EXIT_CODE=$?
    TEMP=$(echo "$GDB_OUTPUT" | grep -qo MIS-MAT)
    # FLASH_NEEDED contains '0' when MIS-MAT was encountered, signaling that a re-flash is needed
    FLASH_NEEDED=$?
    if [ $GDB_EXIT_CODE -ne 0 ] ; then
        echo "$GDB_OUTPUT" >&2
        fail "Reading flash memory failed."
    else
        return $FLASH_NEEDED
    fi
}

#=================================================

while getopts "hi:s:p:b:x:c:C:fnerd" opt; do
  case $opt in
    h)  usage
        exit 0
        ;;
    i)  IMAGE=$(realpath $OPTARG)
        ;;
    s)  SOURCEDIR=$(realpath $OPTARG)
        ;;
    p)  PROBE="$OPTARG"
        if [[ "$PROBE" =~ : ]] ; then
            if ! (exec 3> /dev/tcp/${PROBE/:/\/}) ; then
                fail "Unable to connect to : $PROBE"
            fi
        elif [ ! -c "$PROBE" ] ; then
            fail "Device not found: $PROBE"
        fi
        ;;
    b)  GDB_EXTRA_CMD="$GDB_EXTRA_CMD -ex 'b $OPTARG'"
        ;;
    x)  GDB_EXTRA_CMD="$GDB_EXTRA_CMD -ex '$OPTARG'"
        ;;
    c)  GDB_PRE_CONFIG="$GDB_PRE_CONFIG -x '$OPTARG'"
        if [ ! -e "$OPTARG" ] ; then
            fail "Config file $OPTARG is not readable."
        fi
        ;;
    C)  GDB_POST_CONFIG="$GDB_POST_CONFIG -x '$OPTARG'"
        if [ ! -e "$OPTARG" ] ; then
            fail "Config file $OPTARG is not readable."
        fi
        ;;
    f)  FORCE_FLASH_FLAG=1
        ;;
    n)  SKIP_FLASH_FLAG=1
        ;;
    e)  INTERACTIVE_FLAG=0
        ;;
    r)  RUN_FLAG=1
        ;;
    d)  DETECT_ONLY=1
        ;;
    *)  fail "Invalid option: -$OPTARG"
        ;;
  esac
done

# If no probe name was given on the command line then auto-detect it
if [ -z "$PROBE" ] ; then
    BASEDIR=/sys/class/tty
    DEV_LIST=$(ls $BASEDIR | grep ttyACM)
    if [ ! -z "$DEV_LIST" ] ; then
        for DEV in $DEV_LIST; do
            DEV_FUNC=$(cat $BASEDIR/$DEV/device/interface)
            if [[ $DEV_FUNC =~  "Black Magic GDB Server" ]] ; then
                PROBE=/dev/$DEV
                if [ $DETECT_ONLY -eq 1 ] ; then
                    echo "== Probe found: $PROBE" >&2
                else
                    break
                fi
            fi
        done
    fi
fi

if [ -z "$PROBE" ] && (exec 3> /dev/tcp/${OPENOCD_PROBE/:/\/}) ; then
    PROBE=$OPENOCD_PROBE
fi

if [ $DETECT_ONLY -eq 1 ] ; then
    echo "Detect only requested; Exiting."
    exit 0
elif [ -z "$PROBE" ] ; then
    fail "No probe found."
else
    echo "== Probe found: $PROBE" >&2
fi

# Do some sanity checking
if [ -z $IMAGE ] ; then
    echo "No ELF image path given."
    usage
    exit 1
elif [ ! -e "$IMAGE" ] ; then
    fail "File $IMAGE is not readable."
elif [ ! -d "$SOURCEDIR" ] ; then
    fail "No valid source directory given: $SOURCEDIR."
elif ! GDB_PATH=$(which $GDB 2> /dev/null) ; then
    fail "GDB binary not found: $GDB"
fi

echo "== GDB binary used: $GDB_PATH"

# -n    do not load .gdbinit
# -q    do not show startup text
GDB_BASE_CMD="$GDB -n -q -d $SOURCEDIR -se $IMAGE"
GDB_CONNECT_CMD="-ex 'target extended-remote $PROBE' -ex 'monitor swdp_scan' -ex 'att 1'"

# Override default config with custom config file, if given on the command line
if [ -z "$GDB_PRE_CONFIG" ] ; then
    GDB_PRE_CONFIG="$GDB_INIT_CMD"
fi

# Add flash command before any other commands, if needed or requested
if [ "$SKIP_FLASH_FLAG" -eq 1 ] ; then
    echo "== Not flashing - forced"
elif [ "$FORCE_FLASH_FLAG" -eq 1 ] ; then
    echo "== Flashing - forced"
    GDB_EXTRA_CMD="-ex 'load' $GDB_EXTRA_CMD"
elif FlashNeeded "$IMAGE" ; then
    echo "== Flashing - image changed"
    GDB_EXTRA_CMD="-ex 'load' $GDB_EXTRA_CMD"
else
    echo "== Not flashing - image is unchanged"
fi

# Add run command as final command, if requested
if [ $RUN_FLAG -eq 1 ] ; then
    GDB_EXTRA_CMD="$GDB_EXTRA_CMD -ex 'run'"
fi

# check if exit after run is requested
if [ $INTERACTIVE_FLAG -eq 0 ] ; then
    GDB_EXTRA_CMD="$GDB_EXTRA_CMD --batch"
    echo "== Non-interactive mode selected; exit when done."
fi

eval "$GDB_BASE_CMD $GDB_PRE_CONFIG $GDB_CONNECT_CMD $GDB_POST_CONFIG $GDB_EXTRA_CMD"

#=== EOF

