# Arm / Cortex debug support script

## arm-cortex.{mk,ld}, Makefile-example

A super handy makefile template that allows you to very easily build a makefile project for your MCU. Just copy Makefile-example to you project's root as Makefile and modify it to your needs. Because all the important Makefile logic is contained in the included .mk, you project's Makefile remains super clean and readable. Also, the template can be used over and over again for any of your projects.

## flash.sh

A tool to flash an elf image and starts gdb.
  * Automatically detects the devicename of a connected Black Magic Probe
  * Detects if the given image needs to be reflashed
  * Starts an interactive gdb session with the application either running or halted

To use it, just run the following example from the root of your project's folder:
```
flash.sh -i my_image.elf
```
NOTE: To be have debugsymbols available during debugging, the image needs to be built withe -g flag. Modify the makefile of your project to include that, if needed.


