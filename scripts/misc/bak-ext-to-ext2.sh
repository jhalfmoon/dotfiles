#!/bin/bash

# sync bakupdisk 1 to backupdisk 2

DISK1=/run/media/beep/bak-ssd-1
DISK2=/run/media/beep/bak-ssd-2
DATE=$(date +%Y%m%d-%H%M)
LOG=/tmp/rsync-${DATE}.log

if ! (mount|grep -q $DISK1 2> /dev/null) ; then
    echo "ERROR: Storage not mounted: $DISK1"
else
    if ! (mount|grep -q $DISK2 2>/dev/null) ; then
        echo "ERROR: Storage not mounted: $DISK2"
    else
        rsync -Wa           --inplace --whole-file --delete-before --progress ${DISK1}/ ${DISK2}/ | tee -a $LOG
       #rsync -Wa --dry-run --inplace --whole-file --delete-before --progress ${DISK1}/ ${DISK2}/
    fi
fi
echo "Logs stored at $LOG"

