#!/usr/bin/env bash

# Source: https://superuser.com/questions/109213/how-do-i-list-the-ssl-tls-cipher-suites-a-particular-website-offers

# Note: OpenSSL requires a port number included in the
SERVER=$1
DELAY=0.25
CIPHERS=$(openssl ciphers 'ALL:eNULL' | sed -e 's/:/ /g')
COUNT=$(echo $CIPHERS | wc -w)

PROXY=''
#PROXY="-proxy 10.1.1.7:8008"

echo
echo Obtained cipher list from $(openssl version). In total $COUNT ciphers will be tested.
echo

for cipher in ${CIPHERS[@]} ;do
    echo -n Testing $cipher...
    result="$(echo -n | openssl s_client $PROXY -cipher "$cipher" -connect $SERVER 2>&1)"
    if [[ "$result" =~ ":error:" ]] ; then
        error="$(echo -n $result | cut -d':' -f6)"
        echo NO
        # If you wish to see the error that openssl generated:
        #echo NO \($error\)
    else
        if [[ "$result" =~ "Cipher is ${cipher}" || "$result" =~ "Cipher    :" ]] ; then
            echo YES
        else
            echo UNKNOWN RESPONSE
        echo $result
        fi
    fi
    sleep $DELAY
done
