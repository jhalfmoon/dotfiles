#!/bin/bash

HERE="$(dirname $(readlink -f $0))"
DEST_DIR="/1/store/backup/legion-5-pro"
echo $DEST_DIR

MarkCaches() {
MYHOME="/home/beep"
#FF_CACHE=`find "$MYHOME/.mozilla/firefox" -maxdepth 1 -type d| grep '.default$'`
# NOTE: cd to $MYHOME so that the globbing in SKIP_DIRS works as it should
cd $MYHOME
SKIP_DIRS=( .cache
            .kde4
            .eagle
            .arduino15
            .ansible
            .wine
            .git
            .wine
            .bundle
            .gem
            .glide
            .vagrant.d
            .docker
            .minikube
            .kube
            .thumbnails
            .vscode
            .tabula
            .vscode-oss
            .platformio
            .stmcufinder
            .venv
            .venv3
            .venv38
            .venv39
            .cargo
            .cf
            .krew
            3d
            go
            opt
            tmp
            wip
            Downloads
            Pictures
            Documents
            STM32Cube
            .wine/drive_c
            .googleearth/Cache
            .npm
            .vim/bundle
            .mozilla/firefox
            .local/share/copyq/copyq/items
            .local/state/vs-kubernetes
            .local/share/baloo
            .local/share/gem
            .local/share/Trash
            .local/share/tracker
            .local/share/virtualenv
            .local/share/qBittorrent
            .local/share/backgrounds
            .local/share/krita
            .config/omniverse-launcher
            .config/skypeforlinux
            .config/quodlibet
            .config/Slack
            .config/session
            .config/libreoffice
            .config/discord
            .config/ExpressLRS\ Configurator/firmwares
            .config/VirtualBox
            .config/packer/plugins
            .config/google-chrome
            .config/BraveSoftware/Brave-Browser/aoo**
            .config/BraveSoftware/Brave-Browser/iod**
            .config/BraveSoftware/Brave-Browser/component_crx_cache
            .config/BraveSoftware/Brave-Browser/Default/Extensions
            .config/BraveSoftware/Brave-Browser/DeferredBrowserMetrics
            .config/BraveSoftware/Brave-Browser/Crash\ Reports
            .config/BraveSoftware/Brave-Browser/Greaselion
            .config/BraveSoftware/Brave-Browser/WidevineCdm
            .config/BraveSoftware/Brave-Browser/BraveWallet
            .config/BraveSoftware/Brave-Browser/OnDeviceHeadSuggestModel
            .config/BraveSoftware/Brave-Browser/GrShaderCache
            .config/BraveSoftware/Brave-Browser/Safe\ Browsing
            .config/BraveSoftware/Brave-Browser/BrowserMetrics
            .config/BraveSoftware/Brave-Browser/extensions_crx_cache
            .config/BraveSoftware/Brave-Browser/Default/Favicons
            .config/BraveSoftware/Brave-Browser/Default/publisher_info_db
            .config/BraveSoftware/Brave-Browser/Default/Service\ Worker
            .config/BraveSoftware/Brave-Browser/Default/IndexedDB
            .config/BraveSoftware/Brave-Browser/Default/Local\ Storage
            .config/BraveSoftware/Brave-Browser/Default/GPUCache
            .config/BraveSoftware/Brave-Browser/Default/blob_storage
            .config/BraveSoftware/Brave-Browser/Default/WebStorage
            .config/BraveSoftware/Brave-Browser/Default/File\ System
            .config/BraveSoftware/Brave-Browser/Profile\ **/Service\ Worker/
            .config/BraveSoftware/Brave-Browser/Profile\ **/IndexedDB/
            .config/BraveSoftware/Brave-Browser/Profile\ **/Local\ Storage/
            .config/BraveSoftware/Brave-Browser/Profile\ **/GPUCache/
            .config/BraveSoftware/Brave-Browser/Profile\ **/blob_storage/
            .config/OpenLens
            .config/Code/Cache
            .config/Code/CachedData
            .config/Code/WebStorage
            .config/Code/GPUCache
            .config/Code/Service\ Worker
            .config/Code/User/workspaceStorage
            .config/Code/User/globalStorage
            .config/Code\ -\ OSS/Cache
            .config/Code\ -\ OSS/User/workspaceStorage
            .config/Code/CachedExtensionVSIXs/
            .config/betaflight-configurator
            .config/blheli-configurator
            .config/epiphany
            .config/Signal
            .phoronix-test-suite
            .mystore
            .nrfutil
            .bundles
            .vagrant-bundle
)
# Mark cache directories as such, so that tar knows that they should be excluded
for DIR in "${SKIP_DIRS[@]}" ; do
    #echo === ${HOME}/${DIR}
    if [ -d "${HOME}/${DIR}" ] ; then
        echo 'Signature: 8a477f597d28d172789f06886806bc55' > "${HOME}/${DIR}/CACHEDIR.TAG"
    else
        echo "${HOME}/${DIR} : does not exist and needn't be added to archive exclusion list." >/tmp/not_found.txt
    fi
done
}


DATE=$(date +%Y%m%d-%H%M%S)

MarkCaches
[ ! -d "$DEST_DIR" ] && mkdir -p $DEST_DIR
cd $DEST_DIR

echo "#=== Backing up pacman db, /etc, /boot/grub* ====="
FILE="$DATE-$(uname -n)-system.tar.zst"
time sudo tar --exclude="publisher_info_db" --exclude-caches-all --one-file-system -c -I "zstd -10 --long" -f $FILE /usr/local/bin /var/lib/pacman/local /etc /boot/refind*.conf /boot/EFI /root/.bash_history
sudo chmod g+rw "$FILE"

echo "#=== Backing up $MYHOME =========================="
FILE="$DATE-$(uname -n)-home.tar.zst"
time tar --exclude="publisher_info_db" --exclude-caches-all --one-file-system -c -I "zstd -10 --long" -f $FILE $MYHOME

ls -ltrh | tail -n 10
echo "Latest files in $DEST_DIR"

