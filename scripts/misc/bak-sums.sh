#/bin/bash

# NOTES
# * Parallel is used instead of xargs te ensure that the output texdt does not get trashed.

# time find /run/media/beep/bak-ssd-2/ -type f ! -name '*.xxh128' -print0 | nl
# xargs -0 -P16 -n2 -0 xxh128sum | tee /1/bak-ssd-2b_$(date -I).xxh128
# nl inputfile.txt | xargs -n 2 -P 8 sh -c './myScript.sh "$1" > logfile-$0'

DATE=$(date +%Y%m%d-%H%M)
UNIT_NAME=bak-ssd-1
UNIT_PATH=/run/media/beep/$UNIT_NAME

BAK_PATH=/1/store/backup
SUM_PATH=$BAK_PATH/sums
SUM_FILE=$SUM_PATH//sums/${UNIT_NAME}_${DATE}-2.xxh128

# ensure checksum folder exists
[[ ! -d ${SUM_PATH} ]] && mkdir -p ${SUM_PATH}

# generate checksums
if [[ -d $UNIT_PATH ]] ; then
    # WARNING: Only use parrallel with SSDs. Using parallel with HDDs will tank transfer rates due to seek times.
    time find ${UNIT_PATH}/ -type f ! -name '*.xxh128' | parallel -j 30 xxh128sum | tee ${SUM_FILE}.tmp
    # remove mount-base-path from file paths and sort them so that the checksum file can be easily diffed
    cat ${SUM_FILE}.tmp | sort -k2 | sed 's@/run/media/[^/]\+/[^/]\+/@@g' > ${SUM_FILE}
    rm ${SUM_FILE}.tmp
else
    echo "ERROR: $UNIT_PATH is not mounted."
    exit 1
fi

