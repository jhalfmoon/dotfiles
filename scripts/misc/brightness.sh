#!/bin/bash

# Run at least once from a terminal, to set the udev rules
# jhalfmoon 20220528

USER=$(whoami)
FILE=/etc/udev/rules.d/backlight.rules
if [[ ! -e $FILE ]] ; then
cat <<EOF | sudo tee /etc/udev/rules.d/backlight.rules &>/dev/null
SUBSYSTEM=="backlight",RUN+="chown $USER /sys/class/backlight/%k/brightness /sys/class/backlight/%k/brightness"
EOF
sudo udevadm control --reload-rules && sudo udevadm trigger
fi

BASE=/sys/class/backlight/amdgpu_bl
DEV=${BASE}0/brightness
if [[ ! -c $DEV ]] ; then
    DEV=${BASE}1/brightness
    if [[ ! -c $DEV ]] ; then
        echo "ERROR: No device found at ${BASE}*"
    fi
fi

LEVEL=$(cat $DEV)
if [[ LEVEL -lt 255 ]] ; then
    echo 255 > $DEV
else
    echo 100 > $DEV
fi

