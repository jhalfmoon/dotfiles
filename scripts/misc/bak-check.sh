#/bin/bash -x

set -x

echo ===============

# NOTES
# * Parallel is used instead of xargs te ensure that the output texdt does not get trashed.

# time find /run/media/beep/bak-ssd-2/ -type f ! -name '*.xxh128' -print0 | nl
# xargs -0 -P16 -n2 -0 xxh128sum | tee /1/bak-ssd-2b_$(date -I).xxh128
# nl inputfile.txt | xargs -n 2 -P 8 sh -c './myScript.sh "$1" > logfile-$0'

generate_sums() {
    local UNIT_PATH=$1
    local SUM_FILE=$2
    local TEMP_FILE=${SUM_FILE}.tmp
    # generate checksums
    if [[ -d $UNIT_PATH ]] ; then
        # WARNING: Only use parrallel with SSDs. Using parallel with HDDs will tank transfer rates due to seek times.
        time find ${UNIT_PATH}/ -type f ! -name '*.xxh128' | parallel -j 30 xxh128sum | tee ${TEMP_FILE}
        # remove mount-base-path from file paths and sort them so that the checksum file can be easily diffed
        cat ${TEMP_FILE} | sort -k2 | sed 's@/run/media/[^/]\+/[^/]\+/@@g' > ${SUM_FILE}
        rm ${TEMP_FILE}
    else
        echo "ERROR: $UNIT_PATH is not mounted."
        exit 1
    fi
}

DATE=$(date +%Y%m%d-%H%M)
UNIT_NAME=bak-ssd-1
UNIT_PATH=/run/media/beep/$UNIT_NAME

generate_sums ${UNIT_PATH} ${UNIT_PATH}/sums.xxh128

exit 0

BAK_PATH=/1/store/backup
SUM_PATH=$BAK_PATH/sums
#SUM_FILE_NEW=$SUM_PATH/sums/${UNIT_NAME}_${DATE}-2.xxh128

# ensure checksum folder exists
[[ ! -d ${SUM_FILE} ]] && mkdir -p ${SUM_PATH}

# get the most recently generated sum file
SUM_FILE_PREV=$(ls -tr $SUM_PATH | grep '.xxh128$' | tail -n1)
generate_sums ${UNIT_PATH} ${SUM_FILE_PREV}.tmp

# compare generated sum file with most recent sum file - they should match
diff -u $SUM_FILE_PREV ${SUM_FILE_PREV}.tmp | tee ${SUM_FILE_PREV}.diff

