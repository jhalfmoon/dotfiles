#!/bin/bash

# Shell font style dump
# jhalfmoon 2022-11-03

BG=40
COLNAME=(' black ' '  red  ' ' green ' ' yellow' '  blue ' 'magenta' '  cyan ' ' white ')
STYLENAME=('regular' 'bold   ' 'dim    ' 'italic ' 'underln' '' '' 'invert ')
LINE_BIG="─────────────────────────────────────────────────────────────────"
SPACE="        "

echo -en "\033[0m"
echo '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_+`-=[]{}\|,./<>?"'"'"
echo '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_+`-=[]{}\|,./<>?"'"'"
echo "${SPACE}┌${LINE_BIG}┐"
for STYLE in 1 0 2 3 4 7  ; do
    echo -en "${STYLENAME[$STYLE]} │"
    for FG in {0..7} ; do
        if [[ STYLE -eq 0 ]] ; then
            echo -en "\033[$((FG+30))m\033[${BG}m ${COLNAME[$FG]}"
        else
            echo -en "\033[$((FG+30))m\033[${STYLE};${BG}m ${COLNAME[$FG]}"
        fi
    done
    echo -e "\033[0m │"
done
echo "${SPACE}└${LINE_BIG}┘"

# Unicode characters:  https://www.w3.org/TR/xml-entity-names/025.html
BLOCK=$(printf '\u2588')
ROW=${BLOCK}${BLOCK}${BLOCK}${BLOCK}
LINE_SMALL="──────────────────────────────────"
echo "${SPACE}┌${LINE_SMALL}┐"
for STYLE in 1 0 2  ; do
    echo -en "        │ "
    for FG in {0..7} ; do
        if [[ STYLE -eq 0 ]] ; then
            echo -en "\033[$((FG+30))m\033[${BG}m$ROW"
        else
            echo -en "\033[$((FG+30))m\033[${STYLE};${BG}m$ROW"
        fi
    done
    echo -e "\033[0m │"
done
echo "${SPACE}└${LINE_SMALL}┘"
