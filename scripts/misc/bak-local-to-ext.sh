#!/bin/bash -x

DISK1=/run/media/beep/bak-ssd-1
DATE=$(date +%Y%m%d-%H%M)
LOG=/tmp/rsync-${DATE}.log

if (mount|grep -q $DISK1 2> /dev/null) ; then
    rsync -Wa           \
        --inplace       \
        --whole-file    \
        --delete-before \
        --progress      \
        --log-file /tmp/rsync.log1                  \
        --exclude="/projects/*/temp"                \
        --exclude="/projects/packer/packer_cache"   \
        -- /1/store/ ${DISK1}/store/ | tee -a $LOG
else
    echo "ERROR: Storage not mounted: $DISK1"
fi
echo "Logs stored at $LOG"
