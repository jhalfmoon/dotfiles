#!/bin/bash

# Workaround for "Settings schema 'org.gnome.shell.overrides' is not installed"

# Source: https://andreaskaris.github.io/blog/linux/org-gnome-shell-overrides/
# More info: https://bugs.launchpad.net/ubuntu/+source/gnome-shell-extension-tiling-assistant/+bug/2008139

cat <<'EOF' > /tmp/org.gnome.shell.overrides.gschema.xml
<?xml version="1.0" encoding="utf-8"?>
<schemalist>
   <!-- unused, change 00_org.gnome.shell.gschema.override instead -->
   <schema id="org.gnome.shell.overrides" path="/org/gnome/shell/overrides/"
          gettext-domain="@GETTEXT_PACKAGE@">
     <key name="attach-modal-dialogs" type="b">
       <default>true</default>
       <summary>Attach modal dialog to the parent window</summary>
       <description>
         This key overrides the key in org.gnome.mutter when running
         GNOME Shell.
       </description>
     </key>

     <key name="edge-tiling" type="b">
       <default>true</default>
       <summary>Enable edge tiling when dropping windows on screen edges</summary>
       <description>
         This key overrides the key in org.gnome.mutter when running GNOME Shell.
       </description>
     </key>

     <key name="dynamic-workspaces" type="b">
       <default>true</default>
       <summary>Workspaces are managed dynamically</summary>
       <description>
         This key overrides the key in org.gnome.mutter when running GNOME Shell.
       </description>
     </key>

     <key name="workspaces-only-on-primary" type="b">
       <default>true</default>
       <summary>Workspaces only on primary monitor</summary>
       <description>
         This key overrides the key in org.gnome.mutter when running GNOME Shell.
       </description>
     </key>

     <key name="focus-change-on-pointer-rest" type="b">
       <default>true</default>
       <summary>Delay focus changes in mouse mode until the pointer stops moving</summary>
       <description>
         This key overrides the key in org.gnome.mutter when running GNOME Shell.
       </description>
     </key>
   </schema>
</schemalist>
EOF
sudo cp /tmp/org.gnome.shell.overrides.gschema.xml /usr/share/glib-2.0/schemas/
sudo glib-compile-schemas /usr/share/glib-2.0/schemas/
