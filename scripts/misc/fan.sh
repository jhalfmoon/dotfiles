#!/bin/bash

# Add the following to sudoers:
# %wheel ALL=(ALL) NOPASSWD: /usr/bin/nvidia-settings

sudo nvidia-settings --display :1.0 -a "[gpu:0]/GPUFanControlState=1" -a "[fan:0]/GPUTargetFanSpeed=$1"

