#!/bin/bash

# Reset sound input/output sources and volume and also configure webcam.
# This is very useful for making sure your teleconference settings are all set in one go.

# jhalfmoon 20220318

# Sound configuartion support
#   * Makes sure easyeffects has been run
#   * Toggles between internal and USB output, if USB output is present
#   * Activates external input, if present, otherwise sets internal input
#   * Unmute active inputs and outputs
#   * Set default volume on active input

HERE="$(dirname $(readlink -f $0))"

# Configure cam settings
CAM_SCRIPT=$HERE/cam-settings.sh
if [[ -e $CAM_SCRIPT ]] ; then
    source $CAM_SCRIPT
fi

# Configure keyboard repeat delay and rate
if (which xset &>/dev/null) ; then
    # reset key repeat delay and frequency, as Ubuntu does not maintain these settings after suspend
    xset r rate 190 45
    # enable key repeat on caps lock, which I use as backspace
    xset r 66
else
    echo "Warning: xset not installed."
fi

# Ensure easyeffects has been started
if (which easyeffects &>/dev/null) ; then
    if ! (ps -ef | grep -q [e]asyeffects &>/dev/null) ; then
        easyeffects --hide-window &>>/dev/null
    fi
fi

INPUTS="$(pactl list short  | sort | awk '{print $2}' | grep 'alsa_input\|easyeffects_source' )"
OUTPUTS="$(pactl list short | sort | awk '{print $2}' | grep 'alsa_output\|easyeffects_sink'  )"

OUT_FILTER=$(echo "$OUTPUTS" | grep 'easyeffects_sink$')
OUT_EXT=$(echo "$OUTPUTS" | grep 'usb-Burr-Brown')
OUT_INT=$(echo "$OUTPUTS" | grep 'pci-0000_06_00.6.analog-stereo$')
#OUT_INT=$(echo "$OUTPUTS" | grep 'HiFi__hw_sofhdadsp__sink$')

IN_FILTER=$(echo "$INPUTS" | grep 'easyeffects_source$')
IN_EXT=$(echo "$INPUTS" | grep 'Samson_Q2U_Microphone-00.analog-stereo$')
IN_INT=$(echo "$INPUTS" | grep 'pci-0000_06_00.6.analog-stereo$')
#IN_INT=$(echo "$INPUTS" | grep 'HiFi__hw_sofhdadsp_6__source$')

IN_CUR=$(pactl get-default-source)
OUT_CUR=$(pactl get-default-sink)

OUT_NEW=''
if [[ -n $OUT_EXT ]] ; then
    # If external device is present, then toggle output between internal and external
    case $OUT_CUR in
        *$OUT_INT*)
            OUT_NEW=$OUT_EXT
            ;;
        *$OUT_EXT*)
            OUT_NEW=$OUT_INT
            ;;
        *)
            echo error
            ;;
    esac
else
    # if no external device is found then default to internal
    OUT_NEW=$OUT_INT
fi

# Select input based on availability, most preferred first
case "$INPUTS" in
    *$IN_FILTER*)
        IN_NEW=$IN_FILTER
        ;;
    *$IN_EXT*)
        IN_NEW=$IN_EXT
        ;;
    *$IN_INT*)
        IN_NEW=$IN_INT
        ;;
    *)
        echo error
        ;;
#    *$IN_FILTER*)
#        SOURCE_NEW=$IN_FILTER
#        ;;
esac

# Set output values
pactl set-default-sink  $OUT_NEW
pactl set-sink-mute     $OUT_NEW 0
pactl set-sink-volume   $OUT_NEW 30000

# Set input values
pactl set-default-source $IN_NEW
[[ $INPUTS =~ $IN_INT    ]] && \
    pactl set-source-volume $IN_INT 55000 && \
    pactl set-source-mute $IN_INT 0
[[ $INPUTS =~ $IN_EXT    ]] && \
    pactl set-source-volume $IN_EXT 65000 && \
    pactl set-source-mute $IN_EXT 0
[[ $INPUTS =~ $IN_FILTER ]] && \
    pactl set-source-volume $IN_FILTER 65000 && \
    pactl set-source-mute   $IN_FILTER 0

# if running interactively, then show available devices
if (tty &>/dev/null) ; then
    echo -e "INPUTS:"
    echo ====================
    echo -e "OLD:   $IN_CUR"
    echo -e "NEW:   $IN_NEW"
    echo
    echo -e "$INPUTS"
    echo
    echo -e "OUTPUTS:"
    echo ====================
    echo -e "OLD:   $OUT_CUR"
    echo -e "NEW:   $OUT_NEW"
    echo
    echo -e "$OUTPUTS"
fi
