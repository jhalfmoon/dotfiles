#!/bin/bash

DEST_DIR='./legion-5-pro'
TAR='sudo tar --exclude-caches --one-file-system -c'
DATE=$(date +%Y%m%d-%H%M%S)

MarkCaches() {
SKIP_DIRS=(
            "/var/lib/libvirt"
            "/var/lib/docker"
            "/var/cache"
)
    # Mark cache directories as such, so that tar knows that they should be excluded
    for DIR in "${SKIP_DIRS[@]}" ; do
        if [[ -d "/${DIR}" ]] ; then
            echo 'Signature: 8a477f597d28d172789f06886806bc55' | sudo tee "${DIR}/CACHEDIR.TAG"
#        else
#            echo "${HOME}/${DIR} : does not exist and needn't be added to archive exclusion list."
        fi
    done
}

MarkCaches
[ ! -d "$DEST_DIR" ] && mkdir -p $DEST_DIR
cd $DEST_DIR

echo "#=== Backing up system =========================="
FILE="$DATE-$(uname -n)-system-full.tar.zst"
time $TAR -I 'zstd -10 --long -T0' -f $FILE /boot /
sudo chown beep:beep $FILE

ls -ltrh | tail -n 10

