#!/bin/bash

# NOTES
# * Boost behaviour differs between amd_pstate modes.
#     @guided, @passive
#       boost:
#         AMD PSTATE Highest Performance: 166. Maximum Frequency: 4.46 GHz.
#         AMD PSTATE Nominal Performance: 119. Nominal Frequency: 3.20 GHz.
#         AMD PSTATE Lowest Non-linear Performance: 41. Lowest Non-linear Frequency: 1.10 GHz.
#         AMD PSTATE Lowest Performance: 15. Lowest Frequency: 400 MHz.
#     @active
#       boost:
#         Total States: 3
#         Pstate-P0:  3200MHz
#         Pstate-P1:  1300MHz
#         Pstate-P2:  1200MHz
#       energy_performance_available_preferences:
#         default
#         performance
#         balance_performance
#         balance_power
#         power
# * When userspace governer is active, frequency can be directly configured
#     echo userspace | tee $CF/policy*/scaling_governor
#     echo 2000000   | tee $CF/policy*/scaling_setspeed
# * To play around with offline CPUs
#     echo 1 | tee /sys/devices/system/cpu/cpu{2..15}/online
# * Best setting when creating a small load, but still want silence, sacrificing
#   responsiveness due to extreme low clockspeeds (aound 400MHz idle on all cores):
#       PSTATE_MODE=passive
#       GOVERNOR=conservative
#       PLATFORM_PROFILE=balanced
#       FOO=/sys/devices/system/cpu/cpufreq/conservative
#       echo 95 | tee $FOO/up_threshold
#       echo 80 | tee $FOO/down_threshold
#       echo 10 | tee $FOO/sampling_down_factor
#       echo 20 | tee $FOO/freq_step
#       echo 0  | tee $FOO/ignore_nice_load

#FMAX=4463000
FMAX=0
FMIN=0
BOOST=1
# cat /sys/firmware/acpi/platform_profile_choices
#   quiet
#   balanced - This runs coolest and does not sacrifice much performance compared to "perfomance" mode
#   balanced-performance
#   performance

MODE=FOO
case $MODE in
    MID)
        # Near maxium performance with okay idle temps
        PSTATE_MODE=active
        GOVERNOR=powersave
        EPP=balance_power
        PLATFORM_PROFILE=balanced
        ;;
    MAX)
        # Near maxium performance with okay idle temps
        PSTATE_MODE=active
        GOVERNOR=ondemand
        EPP=power
        PLATFORM_PROFILE=performance
        ;;
    *)
        # Best idle temps but mediocre responsiveness
        PSTATE_MODE=passive
        GOVERNOR=conservative
        EPP=balance_power
        PLATFORM_PROFILE=balanced
        ;;
esac

# PSTATE_MODE
#   active
#   passive
#   guided

# GOVERNOR
# cat /sys/devices/system/cpu/cpufreq/policy0/scaling_available_governors
# passive, guided:
#   powersave
#   schedutil
#   conservative
#   ondemand
#   performance
#   userspace
# active:
#   powersave
#   performance

# EPP
# cat /sys/devices/system/cpu/cpufreq/policy0/energy_performance_available_preferences
#   default
#   performance
#   balance_performance
#   balance_power
#   power

CF=/sys/devices/system/cpu/cpufreq
PSTATE=/sys/devices/system/cpu/amd_pstate/status
PP=/sys/firmware/acpi/platform_profile

echo $PSTATE_MODE > $PSTATE
if [[ -e /sys/devices/system/cpu/cpufreq/policy0/energy_performance_preference ]] ; then
    echo $EPP | tee $CF/policy*/energy_performance_preference >/dev/null
fi
echo $GOVERNOR | tee $CF/policy*/scaling_governor >/dev/null

# First switch to an intermediate platform mode, one that is never used, to
# trigger the fans to stop.
echo balanced-performance | sudo tee $PP
sleep 1
echo $PLATFORM_PROFILE | sudo tee $PP
if [[ -e $CF/boost ]] ; then
    echo $BOOST > $CF/boost
fi

#echo schedutil | tee $CF/policy0/scaling_governor
#echo schedutil | tee $CF/policy1/scaling_governor

[[ $FMAX -gt 0 ]] && echo $FMAX | tee /sys/devices/system/cpu/cpufreq/policy*/scaling_max_freq
[[ $FMIN -gt 0 ]] && echo $FMIN | tee /sys/devices/system/cpu/cpufreq/policy*/scaling_min_freq

# https://wiki.archlinux.org/title/CPU_frequency_scaling
# https://www.kernel.org/doc/Documentation/cpu-freq/governors.txt
if [[ "$GOVERNOR" == "conservative" ]] ; then
    FOO=/sys/devices/system/cpu/cpufreq/conservative
    echo 90 > $FOO/up_threshold
    echo 80 > $FOO/down_threshold
    echo 10 > $FOO/sampling_down_factor
    echo 20 > $FOO/freq_step
    echo 0  > $FOO/ignore_nice_load
fi

# Defaults for Linux 6.6.3
#FOO=/sys/devices/system/cpu/cpufreq/conservative
#echo 75 | tee $FOO/up_threshold
#echo 10 | tee $FOO/down_threshold
#echo 2  | tee $FOO/sampling_down_factor
#echo 10 | tee $FOO/freq_step
#echo 1  | tee $FOO/ignore_nice_load
