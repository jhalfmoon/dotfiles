#!/bin/bash

# jhalfmoon 20160224
# zram swap management script
# Can be used in combination with systemd. See below for example service file.

# NOTE:
# - zram code of a certain age and older is very broken. It contains a race condition that prevents a simple modprobe/udev-rule solution from working. Hence this script.
# - This script works both for systemd and rc based systems
# - $SIZE is the size of each created swap disk. Older versions of the kernel, as used in Centos 5 (2.6.32-573.18.1.el6.x86_64) do not accept human readable numbers, like 512M or 1G, but only whole byte counts. So for the sake of older kernels, this variable is expressed in strictly numerical terms.
# - This script assumes two zswap devices will be created, each with a size of half the total amount of RAM available

#TOTAL_MB=$(grep MemTotal /proc/meminfo | awk '{print $2}' | xargs -I {} echo "scale=0; {}/1024^1" | bc)
TOTAL_KBYTES=$(grep MemTotal /proc/meminfo | awk '{print $2}')
SIZE_BYTES=$((TOTAL_KBYTES*1024))
CONFIG=/etc/modprobe.d/zram.conf
BASEPATH_1=/sys/block
BASEPATH_2=/sys/devices/virtual/block
ALGO='zstd'
#ALGO='lz4'
RETURNCODE=0
DEVICECOUNT=1

usage() {
	echo "Usage: $0 install | <on|off zramX>"
    echo "Default algorithm: $ALGO"
}

ACTION=$1
DEVICE=$2

case $ACTION in
on)
    if [ $# -ne 2 ] ; then
        usage
        RETURNCODE=1
    elif [ ! -r $CONFIG ] ; then
        echo "ERROR: $CONFIG does not exist. You probably should run '$@ install'."
        RETURNCODE=1
    elif ! ( modprobe zram ; ls /dev | grep -q $DEVICE ) ; then
        echo "ERROR: /dev/$DEVICE does not exist"
        RETURNCODE=1
    fi
    [ -r $BASEPATH_1/$DEVICE/comp_algorithm ] && echo $ALGO > $BASEPATH_1/$DEVICE/comp_algorithm
    [ -r $BASEPATH_2/$DEVICE/comp_algorithm ] && echo $ALGO > $BASEPATH_2/$DEVICE/comp_algorithm
    if ! (echo $SIZE_BYTES | sudo tee /sys/block/$DEVICE/disksize) ; then
        echo "ERROR: Failed to configure zram module."
        RETURNCODE=1
    elif ! (mkswap /dev/$DEVICE && swapon -p 1000 /dev/$DEVICE) ; then
        echo "ERROR: Failed to create/activate swap file."
        RETURNCODE=1
    fi
    ;;
off)
    if [ $# -ne 2 ] ; then
        usage
        RETURNCODE=1
    else
        swapoff /dev/$DEVICE
        [ -e $BASEPATH_1/$DEVICE/reset ] && echo 1 > $BASEPATH_1/$DEVICE/reset
        [ -e $BASEPATH_2/$DEVICE/reset ] && echo 1 > $BASEPATH_2/$DEVICE/reset
        # On kernel 5.7.2 an rmmod during shutdown on Arch linux will cause a kernel panic. It's not necessary anyway, so don't do it.
        # rmmod zram
    fi
    ;;
install)
    cp -a $0 /usr/local/bin/zramswap.sh
    chmod +x /usr/local/bin/zramswap.sh
	echo "options zram num_devices=${DEVICECOUNT}" > /etc/modprobe.d/zram.conf
    while [ $DEVICECOUNT -ne 0 ] ; do
        DEVICECOUNT=$((DEVICECOUNT-1))
        if (which systemctl > /dev/null 2>&1) ; then
            cat > /etc/systemd/system/zramswap\@zram${DEVICECOUNT}.service << EOF_UNIT
[Unit]
Description=zram swap configurator
After=local-fs.target

[Service]
ExecStart=/usr/local/bin/zramswap.sh on %I
ExecStop=/usr/local/bin/zramswap.sh off %I
RemainAfterExit=yes

[Install]
WantedBy=local-fs.target
EOF_UNIT
            systemctl daemon-reload
            systemctl enable zramswap@zram${DEVICECOUNT}
            systemctl start zramswap@zram${DEVICECOUNT}
        else
            echo "/usr/local/bin/zramswap.sh on zram${DEVICECOUNT}" >> /etc/rc.local
            /usr/local/bin/zramswap.sh on zram${DEVICECOUNT}
        fi
    done
	;;
*)
    usage
    exit 1
    ;;
esac

exit $RETURNCODE

# EOF
