#!/bin/bash

BASE=/sys/devices/system/cpu/
FREQ=$BASE/cpufreq
IDLE=$BASE/cpuidle

LONG=0

run() {
    if [[ $LONG -eq 1 ]] ; then
        cat /proc/cmdline | tr " " "\n"
        echo ===
        cd /sys/devices/system/cpu/cpufreq/policy0
        ls  | while read ding ; do echo -n "$ding - " ; cat $ding; done
        echo ===
    fi
    sensors k10temp-pci-00c3 zenpower-pci-00c3 amdgpu-pci-0600
    echo ===
    echo "amd_pstate status            : $(cat $BASE/amd_pstate/status)"
    if [[ -e  $FREQ/boost ]] ; then
    echo "boost status                 : $(cat $FREQ/boost)"
    fi
    echo "scaling_driver               : $(cat $FREQ/policy*/scaling_driver             | uniq -c | sed 's/^ *//g')"
    echo "scaling_min_freq             : $(cat $FREQ/policy*/scaling_min_freq           | uniq -c | sed 's/^ *//g')"
    echo "scaling_available_governors  : $(cat $FREQ/policy*/scaling_available_governors| uniq -c | sed 's/^ *//g')"
    echo "scaling_governor             : $(cat $FREQ/policy*/scaling_governor           | uniq -c | sed 's/^ *//g')"
    if [[ -e $FREQ/policy0/energy_performance_preference ]] ; then
    echo "energy_performance_preference: $(cat $FREQ/policy*/energy_performance_preference | uniq -c | sed 's/^ *//g')"
    fi
    echo "idle current_governor        : $(cat $IDLE/current_governor)"
    echo "idle available_governors     : $(cat $IDLE/available_governors)"
    echo ===
    cpupower frequency-info
    echo ===
    cpupower monitor
}

while true ; do
    #DATA=$(su -c "$(declare -f run); run")
    DATA=$(run)
    clear
    echo "$DATA"
    free -m
    sleep 2
done
