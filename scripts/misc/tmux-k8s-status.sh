#!/bin/bash

# 20230510 jhalfmoon
#
# k8s status line for use with tmux
# Requires:
#   kubectl
#   socat

RESET="#[default]"
GREEN="#[fg=#66aa66]"
ORANGE="#[fg=#cc8844]"
YELLOW="#[fg=#cccc44]"
RED="#[bg=#ee5555]"
WHITE="#[fg=#888888]"

# Use icon to signal default GW status
GW=$(ip r | grep '^default' 2> /dev/null | cut -d' ' -f3)
if [ -z "$GW" ] ; then
    ICON="❌"
# Note: This script is called every so many seconds by tmux. Using ping in this script
# will increase CPU wakeups/second from 1000 to 2000 and thus lower battery life. Not worth it.
#elif ! (ping -c1 -W1 $GW &> /dev/null) ; then
#    ICON="⭕"
else
    ICON="📦"
fi

if ! (which kubectl > /dev/null) ; then
    STRING="${ICON} ${WHITE}<kubectl not installed>${RESET}"
elif ! (which socat > /dev/null) ; then
    STRING="${ICON} ${WHITE}<socat not installed>${RESET}"
else
    # Get namespace and cluster name from kubeconfig. Running jq a few times is much faster than
    # running kubectl. The gain is about 50ms per call, which is better for battery life.
    DATA=$(kubectl config view --minify -o json) 2> /tmp/err
    KUBECTL_ERROR=$(cat /tmp/err)
    CLUSTER=$(echo "$DATA"| jq .contexts[].name | tr -d '"')
    NS=$(echo "$DATA"| jq .contexts[].context.namespace | tr -d '"')

    # If namespace is not populated then assume 'default'
    [[ "$NS" == 'null' ]] && NS=default

    # Get API server IP and PORT from current k8s context
    read DUMMY IP PORT <<< $(echo "$DATA" | jq .clusters[].cluster.server | tr -d '"/' | tr : ' ' )

    # Dirty hack to determine if either http_proxy or HTTP_PROXY is set.
    # This is needed because socat wants its proxy to be explicitely set.
    [[ -n $http_proxy ]] && PROXY=$http_proxy
    [[ -n $HTTP_PROXY ]] && PROXY=$HTTP_PROXY

    # Assume no proxy should be used for local IP addressses
    [[ $IP =~ ^10. ]] && PROXY=''
    [[ $IP =~ ^192. ]] && PROXY=''

    # Test if API server TCP connection can be made. Socat is used instead of kubectl
    # as it only needs to test for an http connection, which is much faster than
    # kubectl and also does not cause any load on the API server.
    if [[ -n $PROXY ]] ; then
        read DUMMY PROXY_IP PROXY_PORT <<< $(echo $PROXY | tr /: ' ')
        timeout 1 socat /dev/null PROXY:$PROXY_IP:$IP:$PORT,proxyport=$PROXY_PORT,connect-timeout=1 &> /dev/null
        RESULT=$?
    else
        socat /dev/null TCP4:$IP:$PORT,connect-timeout=0.5 &> /dev/null
        RESULT=$?
    fi

    # If kubectl threw an error...
    if [[ -n $KUBECTL_ERROR ]] ; then
        STRING="${ICON} ${WHITE}<kubectl: ${KUBECTL_ERROR}>${RESET}"
    # If kubeconfig has no contexts defined...
    elif [[ -z $CLUSTER ]] ; then
        STRING="${ICON} ${WHITE}<No context in kubeconfig>${RESET}"
    # If the connection to the API server failed...
    elif [[ $RESULT -ne 0 ]] ; then
        STRING="${ICON} ${WHITE}<$CLUSTER: No connection>${RESET}"
    # Highlight the name of particular clusters
    else
        if [[ $CLUSTER =~ "production" ]] ; then
            COL=$RED
        elif [[ $CLUSTER =~ "staging" ]] ; then
            COL=$ORANGE
        else
            COL=$GREEN
        fi
        STRING="${GREEN}${NS}${RESET} ${ICON} ${COL}${CLUSTER}${RESET}"
    fi
fi

echo -n "${STRING}  "

# debugg
#date >> /tmp/foo
