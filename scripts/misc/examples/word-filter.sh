#!/bin/bash

# jhalfmooon 20240201

if [[ $# -ne 1 ]] ; then
    echo ERROR: No filename given.
    exit 1
fi

SOURCE_FILE=$1
DATA=$(cat "$SOURCE_FILE")
SHORTNAME=$(basename "$SOURCE_FILE" | cut -d. -f1 | tr ' ' '-')

DIR=/tmp/words/$SHORTNAME
LINES_FILE=$DIR/lines.txt
WORDS1_FILE=$DIR/words.txt
WORDS2_FILE=$DIR/words_unique.txt

[[ ! -d $DIR ]] && mkdir -p $DIR

# Filter out lines
#   transform linefeeds to unix style
#   remove special control constructions found in project gutenberg texts: \WORD{PARAMETERS}
#   replace weird quote characters with normal quote
#   keep only selected characters
#   delete % character as sed seems to ignore it
#   delete lines with no letters
#   squash duplicate whitespace and replace with a single space
#   remove leading spaces
#   remove trailing spaces
#   remove empty lines
LINES=$(echo "$DATA"                | \
dos2unix                            | \
sed 's@\\.\+}@@g'                   | \
sed "s@‘\|’\|\`@'@g"                | \
sed 's@[^a-zA-Z0-9.,:;?! --]@@g'    | \
tr -d '%'                           | \
sed 's@^[^a-zA-Z]\+$@@g'            | \
sed 's@\s\+@ @g'                    | \
sed 's@^\s\+@@g'                    | \
sed 's@\s\+$@@g'                    | \
grep -v "^$"                        \
)

# Filter out all punctuation and keep only words, allowing duplicates for representative word frequency
#   spaces to newlines
#   all to lower case
#   remove all but letters, hyphens and apostrophes
#   remove all words not beginning and ending with letters
#   remove empty lines
#   newlines to spaces
WORDS1=$(echo "$LINES"                      | \
tr ' ' '\n'                                 | \
tr '[:upper:]' '[:lower:]'                  | \
sed 's@[^a-zA-Z'\'-']@@g'                   | \
grep -v '\(^[^a-zA-Z]\)\|\([^a-zA-Z]$\)'    | \
grep -v "^$"                                | \
tr '\n' ' '                                 \
)

# De-duplicated words
WORDS2=$(echo "$WORDS1" | \
tr ' ' '\n'             | \
sort                    | \
uniq                    | \
grep -v "^$"            | \
tr '\n' ' '             \
)

echo "$LINES"  > $LINES_FILE
echo "$WORDS1" > $WORDS1_FILE
echo "$WORDS2" > $WORDS2_FILE

echo Wordcount of output files:
wc -w $WORDS1_FILE
cat $WORDS1_FILE | tr ' ' '\n' | head -n 20 | tr '\n' ' '
echo -e '\n..'
cat $WORDS1_FILE | tr ' ' '\n' | tail -n 20 | tr '\n' ' '
echo
echo

wc -w $WORDS2_FILE
cat $WORDS2_FILE | tr ' ' '\n' | head -n 20 | tr '\n' ' '
echo -e '\n..'
cat $WORDS2_FILE | tr ' ' '\n' | tail -n 20 | tr '\n' ' '
echo
echo

cat $WORDS2_FILE | xclip -sel clip -i
