#!/bin/bash -eux

# Sync subrepos using remote name instead of full URLs

sync() {
    local NAME=$1
    local PROJECT=$2
    local URL=$2/$1.git
    local BRANCH=$3

    local DIR=$BASE/$NAME
    local REMOTES="$(git remote | tr '\n' ' ')"

    # Add the remote if it does not yet exist
    if [[ ! "$REMOTES" =~ $NAME ]] ; then
        echo "Adding remote for $NAME"
        git remote add -f $NAME $URL
        git subtree add --prefix $DIR $NAME $BRANCH
    fi

    # Pull if new, else fetch (=update)
    if [[ -d $DIR ]] ; then
        echo "Pulling repo $NAME"
        git subtree pull --prefix $DIR $NAME $BRANCH
    else
        echo "Fetching repo $NAME"
        git fetch $NAME $BRANCH
    fi
}

BASE=repo-backups
[[ -d $BASE ]] || mkdir -p $BASE

sync docker-registry-proxy              https://github.com/jhalfmoon    coreweave
sync nginx-proxy-connect-stable-alpine  https://github.com/jhalfmoon    master
sync ngx_http_proxy_connect_module      https://github.com              master

