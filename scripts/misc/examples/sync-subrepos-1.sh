#!/bin/bash -eu

# Add and / or update a set of git subtrees

sync() {
    local NAME=$1
    local PROJECT=$2
    local BRANCH=$3

    local URL=$PROJECT/$NAME.git
    local DIR=$BASE/$NAME
    local REMOTES="$(git remote | tr '\n' ' ')"

    # Pull if new, else fetch
    if [[ -d $DIR ]] ; then
        echo "Pulling repo $NAME"
        git subtree pull --prefix $DIR $URL $BRANCH
    else
        echo "Adding and fetching repo $NAME"
        # NOTE: Adding a subtree automatically fetches it
        git subtree add --prefix $DIR $URL $BRANCH
    fi
    echo
}

BASE=repo-backups
[[ -d $BASE ]] || mkdir -p $BASE

sync docker-registry-proxy              https://github.com/jhalfmoon    coreweave
sync nginx-proxy-connect-stable-alpine  https://github.com/jhalfmoon    master
sync ngx_http_proxy_connect_module      https://github.com/chobits      master

