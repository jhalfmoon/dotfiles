#!/bin/bash
#
# 20240704
# Fetch a gitlab asset for a given project, based on latest known release version

PROJ_NAME=gitlab-org/cli
ASSET_FORMAT=Linux_x86_64.tar.gz
BASE=https://gitlab.com/api/v4/projects

if ! (which jq &>/dev/null) ; then
    echo "ERROR: jq binary is not found in PATH. Please install it before running this script."
    exit 1
fi

# URLify the project name - https://stackoverflow.com/questions/296536/how-to-urlencode-data-for-curl-command
TEMP=$(echo -n $PROJ_NAME | jq -sRr @uri)
echo "Fetching project ID..."
PROJ_ID=$(curl -s $BASE/$TEMP | jq .id)

echo "Fetching latest release version..."
LATEST_REL=$(curl -s $BASE/$PROJ_ID/releases/ | jq '.[]' | jq -r '.name' | head -1)

echo "Fetching list of assets related to latest release..."
ASSETS=$(curl -s $BASE/$PROJ_ID/releases/$LATEST_REL)

# Filter out the required url
ASSET_URL=$(echo "$ASSETS" | jq ".assets.links[] | select(.name | contains(\"$ASSET_FORMAT\"))" | jq -r .direct_asset_url)

echo
echo "Project name:   $PROJ_NAME"
echo "Asset format:   $ASSET_FORMAT"
echo
echo "Project ID :    $PROJ_ID"
echo "Latest version: $LATEST_REL"
echo "Asset URL:      $ASSET_URL"

if [[ -n "$ASSET_URL" ]] ; then
    echo
    echo "Fetching asset..."
    curl -LO "$ASSET_URL"
else
    echo "ERROR: No asset name found that matches the request format $ASSET_FORMAT"
    exit 1
fi

