#!/bin/bash -u

# 20181013 jhalfmoon
# Battery status script for use with tmux
#
# Example usage from a shell:
# USE_TMUX=0 WARN_PCT=10 CRIT_PCT=5 ./battery-status.sh
#
# Example usage in a tmux config:
# set -g status-interval 10
# set -g status-right-length 50
# set -g status-right "#(~/dotfiles/support/tmux-battery-status.sh) %-d-%b %-H:%M"

# NOTES:
# - Using icons requires installing noto fonts and configuring font.conf to use the emoji font as fallback.
#   See https://raw.githubusercontent.com/TheBunnyMan123/config-files/main/fontconfig/fonts.conf
# - The path to the battery stats may differ per machine. You may have to adjust it for your particular machine.

#exit 0

# Set default parameters. These can be given as environment variables to modify this script's behaviour
[ ! -v CRIT_PCT ] && CRIT_PCT=15
[ ! -v WARN_PCT ] && WARN_PCT=35
[ ! -v USE_TMUX ] && USE_TMUX=1

ICON_BAT="🔋"
ICON_PLUG="🔌"

# The thermometer icon confuses kitty when using it in the tmux status bar d.d. 20231025
#ICON_THERM="🌡️"
ICON_THERM=""

CHARGE_FULL=''
CHARGE_NOW=''
VOLTAGE_NOW=''
CURRENT_NOW=''

BAT_BASE=/sys/class/power_supply

# select either bash or tmux color codes
if [ $USE_TMUX -eq 1 ] ; then
    _GREEN="#[fg=#66cc66]"
    _ORANGE="#[fg=#cc8844]"
    _YELLOW="#[fg=#cccc44]"
    _RED="#[fg=#ff6666]"
    _RESET="#[fg=white]"
else
    _GREEN='\033[1;32m'
    _YELLOW='\033[1;33m'
    _ORANGE='\033[33m'
    _RED='\033[1;31m'
    _RESET='\033[0m'
fi

if [[ -e $BAT_BASE/BAT0/energy_full ]] ; then
    PREFIX=energy
elif [[ -e $BAT_BASE/BAT0/charge_full ]] ; then
    PREFIX=charge
else
    PREFIX=nobat
fi

    if [[ "$PREFIX" == "nobat" ]] ; then
        BAT_STRING="${_ORANGE}noBAT"
    else
    # accumulate all battery values and treat them as one battery, as some machines have multiple batteries
    while read i ; do CHARGE_FULL=$((CHARGE_FULL+$(cat $i))); done < <(ls $BAT_BASE/BAT*/${PREFIX}_full)
    while read i ; do CHARGE_NOW=$((CHARGE_NOW+$(cat $i))); done   < <(ls $BAT_BASE/BAT*/${PREFIX}_now)
    while read i ; do VOLTAGE_NOW=$((VOLTAGE_NOW+$(cat $i))); done < <(ls $BAT_BASE/BAT*/voltage_now)
    while read i ; do CURRENT_NOW=$((CURRENT_NOW+$(cat $i))); done     < <(ls $BAT_BASE/BAT*/${PREFIX}_now)
    REMAINING_PERCENT=$(bc <<< "scale=4; (($CHARGE_NOW*100) / $CHARGE_FULL) + 0.49" | cut -d'.' -f1)
    [[ $CURRENT_NOW -gt 0 ]] && REMAINING_SECONDS=$(bc <<< "scale=4; ($CHARGE_NOW / $CURRENT_NOW) * 3600" | cut -d'.' -f1) || REMAINING_SECONDS=0
    STATE='' ; while read i ; do STATE=${i}_$STATE; done < <(cat $BAT_BASE/BAT*/status)
    WATT=$(bc <<< "scale=4; $CURRENT_NOW*$VOLTAGE_NOW / 1000000000000" | sed 's/^\./0./' | cut -d'.' -f1)

    # debugging code; not used by default
    #date >> /tmp/bat.log
    if false; then
        CHARGE_FULL=1000
        CHARGE_NOW=500
        POWER=750
        STATE='Disch'
    fi
    if false; then
        echo "full $CHARGE_FULL"
        echo "now $CHARGE_NOW"
        echo "cur $POWER"
        echo "state $STATE"
    fi

    BAT_STRING=""
    # Determine display color, which will be determined by charge percentage
    if [ -v REMAINING_PERCENT ] ; then
        if [ $REMAINING_PERCENT -lt $CRIT_PCT ] ; then
            COLOR=$_RED
        elif [ $REMAINING_PERCENT -lt $WARN_PCT ]; then
            COLOR=$_ORANGE
        else
            COLOR="$_GREEN"
        fi
    fi

    # generate the actual string that will be displayed
    if [[ $STATE =~ .*harg.* ]] ; then
        # calculate remaining time
        i=$REMAINING_SECONDS
        ((sec=i%60, i/=60, min=i%60, hrs=i/60))
        REMAINING_TIME=$(printf "%dh%02dm" $hrs $min)
        if [[ $STATE =~ Disch.* ]] ; then
            BAT_STRING="${ICON_BAT}${COLOR}BAT[${REMAINING_PERCENT}% ${WATT}W ${REMAINING_TIME}]${_RESET}"
        elif [[ $STATE =~ Charg.* ]] ; then
            BAT_STRING="${ICON_PLUG}${COLOR}Charging[${REMAINING_PERCENT}% ${WATT}W ${REMAINING_TIME}]${_RESET}"
        elif [[ $STATE =~ 'Not charging' ]] ; then
            BAT_STRING="${ICON_PLUG}${COLOR}Idle[${REMAINING_PERCENT}%]${_RESET}"
        else
            BAT_STRING="${_YELLOW}BAT[??]${_RESET}"
        fi
    elif [[ $STATE =~ ^Full.* ]] ; then
        BAT_STRING="${_GREEN}AC[100%]${_RESET}"
    else
        # Any other state (usually 'unknown') indicates a charge status of less than 100%, but no charging is being done
        BAT_STRING="${COLOR}Idle[${REMAINING_PERCENT}% ${WATT}W]${_RESET}"
    fi
fi

AMD1=k10temp-pci-00c3
AMD2=zenpower-pci-00c3
INTEL1=pch_cannonlake-virtual-0
INTEL1=coretemp-isa-000

if (which sensors > /dev/null) ; then
    # Get cPU temperature. First for the k10 module...
    SENSOR1=$(sensors -u $AMD1 2>  /dev/null | grep temp1       | cut -d: -f2 | cut -d'.' -f1 | tr -cd [:digit:])
    # and when zenpower module is loaded
    SENSOR2=$(sensors -u $AMD2 2> /dev/null | grep temp1_input | cut -d: -f2 | cut -d'.' -f1 | tr -cd [:digit:])
    # intel
    SENSOR3=$(sensors -u $INTEL1 2> /dev/null | grep temp1_input | cut -d: -f2 | cut -d'.' -f1 | tr -cd [:digit:])
    SENSOR4=$(sensors -u $INTEL2 2> /dev/null | grep temp1_input | cut -d: -f2 | cut -d'.' -f1 | tr -cd [:digit:])
    TEMP_STRING="${_YELLOW}${ICON_THERM}${SENSOR1}${SENSOR2}${SENSOR3}${SENSOR4}°"
else
    TEMP_STRING="${_RED}<lm-sensors missing>"
fi

echo -ne "${_GREEN}${BAT_STRING}${_RESET} ${TEMP_STRING}"

