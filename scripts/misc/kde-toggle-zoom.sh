#!/bin/bash -eu

# 20240923 jhalfmoon
# This script toggles the kde zoom accesibility feature. There is currently no built in toggle key for this
# feature. Having zoom permanently enabled may create performance issues. For example, copyq will react about
# one second slower when opening the clipboard history using a hotkey. That is why this script was created.
#
# Note that using qdbus to trigger the change does take about half a second, which I find a painfully long
# delay for activating zoom, but I currently have no better alternative.

check_file() {
    if ! (which $1 &>/dev/null) ; then
        echo "ERROR: File $1 not found in PATH."
        return 1
    fi
}

check_file kreadconfig5  || exit 1
check_file kwriteconfig5 || exit 1
check_file qdbus         || exit 1

PARAMS="--file $HOME/.config/kwinrc --group Plugins --key zoomEnabled"

STATUS=$(kreadconfig5 ${PARAMS[@]})
case $STATUS in
    false)
        NEWSTATUS=true;;
    *)
        NEWSTATUS=false;;
esac

kwriteconfig5 ${PARAMS[@]} "$NEWSTATUS" && qdbus org.kde.KWin /KWin reconfigure

