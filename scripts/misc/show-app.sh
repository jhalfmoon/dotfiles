#!/bin/bash
#
# Bring an application windows to current desktop and to front if it is already running.
# Otherwise start it.
# This is needed because keepassxc will not come to your current desktop if it is
# already running and it is located on another desktop than your current one.

APP=$1
if [[ $# -ne 1 ]] ; then
    echo ERROR: This script requires exactly one parameter. Received: $#
    exit 1
elif ! (which wmctrl &> /dev/null ) ; then
    echo ERROR: wmctrl is not installed.
    exit 1
elif ! (which $APP &> /dev/null ) ; then
    echo ERROR: The application $APP is not found in \$PATH.
    exit 1
fi
wmctrl -R $APP || $APP
