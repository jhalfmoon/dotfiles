#!/bin/bash -u

# 20250130 kvvliet

# This tools will clone all available repositories from a gitlab server.
# * If a repo has already been cloned before, a git fetch will be done.
# * Configuation of this script is done using an external file defined in $CONIG_FILE
# * To access the gitlab API, a token with API read access is required.
# * The list of repos depends on what repos are visible for the given $API_TOKEN.
# * If $REPO_TOKEN is defined and $FORCE_SSH_CLONE is 0, cloning will be done over
#   https. $REPO_TOKEN must have repo-read access. In any other case ssh will be used.
# * Use ssh for cloning when possible, as using http will make it that the token will be
#   available in the .git/config file of each repo, after cloning, which is a security risk.

HERE=$(dirname $(readlink -f $0))
CONIG_FILE=gitlab-sync.conf
USE_HTTP=0

# Silence MOTD messages
export GIT_SSH_COMMAND="ssh -oLogLevel=QUIET"

# Delete dir when debugging / testing
#rm -rf $DEST_DIR

if [ ! -e $HERE/$CONIG_FILE ] ; then
    echo "ERROR: Config file $HERE/$CONIG_FILE not found."
    echo "       The file should contain variable definitions for TOKEN and DEST_DIR."
    exit 1
fi
source $HERE/$CONIG_FILE

if [[ -z $DEST_DIR ]] ; then
    echo "ERROR: DEST_DIR not set."
    exit 1
fi
[[ -d $DEST_DIR ]] || mkdir -p $DEST_DIR
cd $DEST_DIR

if [[ -z $REPO_TOKEN ]] ; then
    echo "WARNING: REPO_TOKEN is not setDEST_DIR not set."
    sleep 3
fi

if [[ -z $API_TOKEN ]] ; then
    echo "ERROR: API_TOKEN not set."
    exit 1
else
    echo "Checking validity of API_TOKEN at $BASE_URL..."
    CODE=$(curl --silent --output /dev/null --write-out "%{http_code}" --header "PRIVATE-TOKEN: $API_TOKEN" "$BASE_URL/projects?per_page=1")
    if [[ ${CODE} -lt 200 || ${CODE} -gt 299 ]] ; then
        echo "ERROR: A curl to $BASE_URL returned HTTP code $CODE."
        echo "       Check that the supplied token and url is correct."
        exit 1
    else
        echo "Check succeeded."
    fi
fi

PAGE=1
while [[ $PAGE -gt 0 ]] ; do
    # Fetch metadata repositories
    echo "=== Fetching page $PAGE of repository metadata"
    DATA=$(curl --silent --header "PRIVATE-TOKEN: $API_TOKEN" "$BASE_URL/projects?per_page=$PER_PAGE&page=$PAGE&simple=true")
    echo "$DATA" > /tmp/file_$PAGE
    # determine amount of projects returned
    LIST_SIZE=$(echo "$DATA" | jq -r .[].name | wc -l)
    echo "    Received $LIST_SIZE repositories."
    PAGE=$((PAGE+1))
    # if list countains more than one project, clone or pull it
    if [[ $LIST_SIZE -lt 1 ]] ; then
        # signal end of run
        PAGE=0
    else
        # iterate over each project in the list
        echo "$DATA" | jq -rC '.[] | "\(.path) \(.path_with_namespace) \(.http_url_to_repo) \(.ssh_url_to_repo)"' | while read NAME FULLPATH HTTP_URL SSH_URL; do
            if [[ $DEBUG -eq 1 ]] ; then
                echo "= NAME:     $NAME"
                echo "  FULLPATH: $FULLPATH"
                echo "  URL:      $HTTP_URL"
                echo
            fi
            cd $DEST_DIR
            if [[ -d $FULLPATH ]] ; then
                echo = Updating $FULLPATH
                cd $FULLPATH
                git fetch --all
                # https://stackoverflow.com/questions/292357/what-is-the-difference-between-git-pull-and-git-fetch
                git merge FETCH_HEAD
            else
                echo "Cloning $(git config --get remote.origin.url)"
                if [[ -z $REPO_TOKEN ]] || [[ $FORCE_SSH_CLONE -eq 1 ]]; then
                    git clone $SSH_URL $FULLPATH
                else
                    # insert gitlab token into url
                    HTTP_URL_AUTH="https://oauth2:$REPO_TOKEN@$(echo $HTTP_URL | cut -d/ -f3-)"
                    git clone $HTTP_URL_AUTH $FULLPATH
                fi
            fi
        done
    fi
done
