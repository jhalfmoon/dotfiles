#!/bin/bash -x

rm -rf "/home/beep/.config/Code/Cache"
rm -rf "/home/beep/.config/Code/CachedExtensionVSIXs"
rm -rf "/home/beep/.config/Code/User/workspaceStorage"

rm -rf "/home/beep/.cache/BraveSoftware/Brave-Browser"
find $HOME/.config/BraveSoftware -type d | grep 'CacheStorage$' | while read DIR ; do rm -rf "$DIR"; done
