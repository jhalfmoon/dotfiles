#!/bin/bash -x
#
# Configure webcam. Requirements:
#   sudo apt install -y v4l-utils guvcview
# Run guvcview to tune the settings, then read settings with:
#   v4l2-ctl -d /dev/video0 -l

# Remove this line to enable this script
#return 0

if ! (which v4l2-ctl &>/dev/null) ; then
    echo ERROR: v4l2-ctl not installed.
fi

# Default to warm
SETTING=${1:-"1"}

case $SETTING in
    4)
        # HP Elitebook 1040 G4
        v4l2-ctl -c brightness=19
        v4l2-ctl -c contrast=60
        v4l2-ctl -c saturation=30
        v4l2-ctl -c hue=0
        v4l2-ctl -c white_balance_automatic=0
        v4l2-ctl -c gamma=187
        v4l2-ctl -c power_line_frequency=1
        v4l2-ctl -c white_balance_temperature=6400
        v4l2-ctl -c sharpness=0
        v4l2-ctl -c backlight_compensation=0
        #
        v4l2-ctl -c auto_exposure=3
        #v4l2-ctl -c exposure_time_absolute=300
        v4l2-ctl -c exposure_dynamic_framerate=1
        ;;
    3)
        # HP Elitebook 1040 G4
        v4l2-ctl -c brightness=19
        v4l2-ctl -c contrast=48
        v4l2-ctl -c saturation=32
        v4l2-ctl -c hue=0
        v4l2-ctl -c white_balance_automatic=0
        v4l2-ctl -c gamma=203
        v4l2-ctl -c power_line_frequency=1
        v4l2-ctl -c white_balance_temperature=6400
        v4l2-ctl -c sharpness=0
        v4l2-ctl -c backlight_compensation=0
        #
        v4l2-ctl -c auto_exposure=3
        #v4l2-ctl -c exposure_time_absolute=300
        v4l2-ctl -c exposure_dynamic_framerate=1
        ;;
    2)
        # HP Elitebook 1040 G4
        v4l2-ctl -c brightness=-7
        v4l2-ctl -c contrast=30
        v4l2-ctl -c saturation=54
        v4l2-ctl -c hue=0
        v4l2-ctl -c white_balance_automatic=0
        v4l2-ctl -c gamma=203
        v4l2-ctl -c power_line_frequency=1
        v4l2-ctl -c white_balance_temperature=4000
        v4l2-ctl -c sharpness=0
        v4l2-ctl -c backlight_compensation=0
        #
        v4l2-ctl -c auto_exposure=3
        #v4l2-ctl -c exposure_time_absolute=300
        v4l2-ctl -c exposure_dynamic_framerate=1
        ;;
    *)
        ;&
    1)
        # HP Elitebook 1040 G4
        v4l2-ctl -c brightness=0
        v4l2-ctl -c contrast=32
        v4l2-ctl -c saturation=64
        v4l2-ctl -c hue=0
        v4l2-ctl -c white_balance_automatic=0
        v4l2-ctl -c gamma=328
        v4l2-ctl -c power_line_frequency=1
        v4l2-ctl -c white_balance_temperature=5960
        v4l2-ctl -c sharpness=0
        v4l2-ctl -c backlight_compensation=0
        #
        v4l2-ctl -c auto_exposure=3
        #v4l2-ctl -c exposure_time_absolute=300
        v4l2-ctl -c exposure_dynamic_framerate=1
        ;;
esac
